using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulser : MonoBehaviour
{
    [SerializeField] float pulseScale = 1.5f;
    [SerializeField] float pulseTime = 1f;

    private bool pulsing;

    private Vector3 startScale;

    private void Awake()
    {
        startScale = transform.localScale;
    }

    public void Pulse(Transform transform)
    {
        if (pulsing)
            return;

        pulsing = true;

        LeanTween.scale(transform.gameObject, startScale * pulseScale, pulseTime / 2f)
                    .setEaseInOutBack()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(transform.gameObject, startScale, pulseTime / 2f)
                                    .setEaseInQuart();
                        pulsing = false;
                    });
    }
}
