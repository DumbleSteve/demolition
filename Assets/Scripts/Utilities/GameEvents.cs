using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Services.Lobbies.Models;

public class GameEvents
{
    // stack building

    public delegate void OnBuildStacksDelegate(List<BlockData> blockList, StackData stackData);
    public static OnBuildStacksDelegate OnBuildStacks;                // start building stacks

    public delegate void OnStackBuiltDelegate(Stack newStack);
    public static OnStackBuiltDelegate OnStackBuilt;                // on completion of building a stack

    public delegate void OnStackPlayerSetDelegate(StackPlayer stackPlayer, Stack stack, Color playerColour);
    public static OnStackPlayerSetDelegate OnStackPlayerSet;        // player assigned to stack on completion of building a stack

    public delegate void OnAllStacksBuiltDelegate(List<Stack> stacks);
    public static OnAllStacksBuiltDelegate OnAllStacksBuilt;        // on completion of all stacks

    public delegate void OnResetStacksDelegate();
    public static OnResetStacksDelegate OnResetStacks;              // rebuild all stacks

    public delegate void OnReloadDataDelegate();
    public static OnReloadDataDelegate OnReloadData;                // destroy blocks, re-fetch stack data and build all stacks

    // turns

    public delegate void OnNextPlayerTurnDelegate();
    public static OnNextPlayerTurnDelegate OnNextPlayerTurn;        // either 'end turn' clicked or charge timed out

    public delegate void OnTurnStartDelegate();
    public static OnTurnStartDelegate OnTurnStart;                  // either 'end turn' clicked or charge timed out

    public delegate void OnTurnChangedDelegate(StackPlayer newTurnPlayer);    // immediately after start of each turn - after next player selected
    public static OnTurnChangedDelegate OnTurnChanged;

    public delegate void OnNetworkStartTurnDelegate(ulong newTurnPlayerId, string stackName, bool isPlayersTurn);    // at start of each turn - fired via ClientRPC to all players
    public static OnNetworkStartTurnDelegate OnNetworkStartTurn;

    public delegate void OnIsLocalPlayersTurnDelegate(bool isLocalPlayersTurn, int numPlayers);    // fired from ClientRPC by NetcodePlayer
    public static OnIsLocalPlayersTurnDelegate OnIsLocalPlayersTurn;

    public delegate void OnTurnEndDelegate(StackPlayer player, bool timedOut);
    public static OnTurnEndDelegate OnTurnEnd;                      // either player detonated or charge timed out

    public delegate void OnManualStackSelectDelegate(Stack stack);
    public static OnManualStackSelectDelegate OnManualStackSelect;    // current stack set by SelectStackButton (doesn't end turn)

    public delegate void OnTurnChargeTickDelegate(int maxDamageCharge, int damageChargeRemaining, bool turnEnded);
    public static OnTurnChargeTickDelegate OnTurnChargeTick;

    public delegate void OnGameOverDelegate(StackPlayer winner);
    public static OnGameOverDelegate OnGameOver;                    // stacks demolished / players have no charge left

    // player

    public delegate void OnChangeMaxChargeDelegate(Stack stack, int chargeIncrement);       // increment may be negative
    public static OnChangeMaxChargeDelegate OnChangeMaxCharge;          // increase/decrease max charge of player that owns this stack

    public delegate void OnMaxChargeChangedDelegate(Stack stack, int maxCharge);
    public static OnMaxChargeChangedDelegate OnMaxChargeChanged;          // player max charge was changed

    public delegate void OnChangeExtraHitsDelegate(Stack stack, int hitsIncrement);      // increment may be negative
    public static OnChangeExtraHitsDelegate OnChangeExtraHits;          // by rune - increase/decrease extra hits (for next turn only) of player that owns this stack

    public delegate void OnExtraHitsChangedDelegate(Stack stack, int extraHits);
    public static OnExtraHitsChangedDelegate OnExtraHitsChanged;          // player extra hits was changed

    // stack management

    public delegate void OnEnablePhysicsDelegate(bool enable, Stack stack);
    public static OnEnablePhysicsDelegate OnEnablePhysics;          // switch isKinematic for all block rigidbodies

    public delegate void OnRestoreBlocksDelegate(Stack stack, bool restoreStrength);
    public static OnRestoreBlocksDelegate OnRestoreBlocks;          // return to start position / rotation

    public delegate void OnDestroyStacksDelegate();
    public static OnDestroyStacksDelegate OnDestroyStacks;          // ready for rebuild

    public delegate void OnFreezeStackDelegate(Stack stack, bool freeze);
    public static OnFreezeStackDelegate OnFreezeStack;              // frozen by rune, unfrozen when player's turn comes around again

    public delegate void OnRestoreStackPoseDelegate();
    public static OnRestoreStackPoseDelegate OnRestoreStackPose;          // current stack - synced over network

    public delegate void OnStackFallenBlocksDelegate(Stack stack, float fallenPercent);
    public static OnStackFallenBlocksDelegate OnStackFallenBlocks;   // on block demolished or fallen

    public delegate void OnOnStackDamagedDelegate(Stack stack, float initialStrength, float currentStrength);
    public static OnOnStackDamagedDelegate OnStackDamaged;                      // whenever a block takes damage

    public delegate void OnDemolishStackDelegate(string stackName);
    public static OnDemolishStackDelegate OnDemolishStack;          // synced over network

    public delegate void OnStackDemolishedDelegate(Stack stack);
    public static OnStackDemolishedDelegate OnStackDemolished;      // when enough blocks 'fallen'

    public delegate void OnStackRankDelegate(Stack stack, int rank);
    public static OnStackRankDelegate OnStackRank;      // when stack demolished

    // blocks

    public delegate void OnBlockHitDepthChangedDelegate(int blocksToDamage);
    public static OnBlockHitDepthChangedDelegate OnBlockHitDepthChanged;          // number of blocks deep from raycast / mouse wheel

    public delegate void OnDetonateDelegate(float damagePerBlock, Rune rune);
    public static OnDetonateDelegate OnDetonate;                                // inflict damage on selected blocks / activate rune

    public delegate void OnDemolishBlockDelegate(int blockId, string runeName); // same runeName across network
    public static OnDemolishBlockDelegate OnDemolishBlock;

    public delegate void OnChangeBlockStrengthDelegate(Stack stack, float totalStrength);
    public static OnChangeBlockStrengthDelegate OnChangeBlockStrength;          // increase/decrease all remaining blocks in stack

    // runes

    public delegate void OnRuneSpawnedDelegate(Rune newRune);           // cache for lookup by block is
    public static OnRuneSpawnedDelegate OnRuneSpawned;

    public delegate void OnRevealRuneDelegate();
    public static OnRevealRuneDelegate OnRevealRune;                    // on block destruction

    public delegate void OnRuneRevealedDelegate(Rune rune);
    public static OnRuneRevealedDelegate OnRuneRevealed;                // on block destruction

    public delegate void OnActivateRuneDelegate();                      // on detonate button, activates selected rune (before OnDetonate ends turn)
    public static OnActivateRuneDelegate OnActivateRune;

    public delegate void OnRuneActivatedDelegate(Rune rune);
    public static OnRuneActivatedDelegate OnRuneActivated;          // by player via raycast and detonate button

    public delegate void OnRuneExpiredDelegate(Rune rune);
    public static OnRuneExpiredDelegate OnRuneExpired;              // if it has an expiry time, else immediately on activation

    // camera

    public delegate void OnCameraOrbitStackDelegate(Stack stack);
    public static OnCameraOrbitStackDelegate OnCameraOrbitStack;    // set the stack that the orbit camera is to look at

    public delegate void OnCameraOrbitTableDelegate();
    public static OnCameraOrbitTableDelegate OnCameraOrbitTable;    // return camera to look at table

    // raycast

    public delegate void OnRaycastHitDelegate(List<StackBlock> hitBlocks, Rune hitRune);
    public static OnRaycastHitDelegate OnRaycastHit;                // spherecast from mouse point on right-mouse (hits multiple blocks or single rune) - both may be null

    public delegate void OnRaycastEndDelegate(List<StackBlock> hitBlocks, Rune hitRune);
    public static OnRaycastEndDelegate OnRaycastEnd;                // right mouse up

    // detonator

    public delegate void OnDetonatorActivatedDelegate(Stack stackToDetonate);
    public static OnDetonatorActivatedDelegate OnDetonatorActivated;        // start push down to detonate

    public delegate void OnDetonatorDownDelegate(Stack stackToDetonate, bool gameStart);
    public static OnDetonatorDownDelegate OnDetonatorDown;                  // pushed down and detonated

    // multiplayer lobby / relay

    public delegate void OnAuthenticatedDelegate(string playerName, string playerId);        // call back after authentication
    public static OnAuthenticatedDelegate OnAuthenticated;

    public delegate void OnAuthenticateErrorDelegate(string error);                    // eg. illegal player name
    public static OnAuthenticateErrorDelegate OnAuthenticateError;

    public delegate void OnCreateLobbyDelegate();
    public static OnCreateLobbyDelegate OnCreateLobby;              // host -> UI form to fill out

    public delegate void OnJoinedLobbyDelegate(Lobby joinedLobby, bool isLobbyHost, string playerName);
    public static OnJoinedLobbyDelegate OnJoinedLobby;              // host and client

    public delegate void OnLobbyUpdatedDelegate(Lobby joinedLobby);
    public static OnLobbyUpdatedDelegate OnLobbyUpdated;              // as a result of polling

    public delegate void OnLobbyListChangedDelegate(List<Lobby> lobbies);
    public static OnLobbyListChangedDelegate OnLobbyListChanged;

    public delegate void OnLobbyGameModeChangedDelegate(Lobby lobby);
    public static OnLobbyGameModeChangedDelegate OnLobbyGameModeChanged;

    public delegate void OnLeftLobbyDelegate();
    public static OnLeftLobbyDelegate OnLeftLobby;                  // host and client

    public delegate void OnKickedFromLobbyDelegate();
    public static OnKickedFromLobbyDelegate OnKickedFromLobby;

    public delegate void OnLobbyStartDelegate(LobbyPlayerData lobbyPlayerData);
    public static OnLobbyStartDelegate OnLobbyStartGame;             // by host from lobby -> on relay created (host) / joined (client) -> hides lobby UI etc

    public delegate void OnAllPlayersLinkedDelegate();
    public static OnAllPlayersLinkedDelegate OnAllPlayersLinked;    // all NetCodePlayers and StackPlayers linked up

    public delegate void OnConstructStackBuildDataDelegate(List<LobbyPlayerData> lobbyPlayerNames);
    public static OnConstructStackBuildDataDelegate OnConstructStackBuildData;  // when all netcode players spawned -> assignes player names/sprites -> use to build stack data

    public delegate void OnReadyToBuildStacksDelegate();
    public static OnReadyToBuildStacksDelegate OnReadyToBuildStacks;  // when all netcode players have been spawned -> construct stack data -> build stacks

    // music

    public delegate void OnToggleMusicMuteDelegate();
    public static OnToggleMusicMuteDelegate OnToggleMusicMute;      // via UI button -> MusicPlayer

    public delegate void OnMusicMutedDelegate(bool muted);
    public static OnMusicMutedDelegate OnMusicMuted;                // via MusicPlayer -> UI button
}
