using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player Data", fileName = "Player Data")]

public class PlayerDataSO : ScriptableObject
{
    public bool HintsHidden = false;        // stay hidden once player hides them (button) - player be re-show any time
    public bool MusicMuted = false;        // stay hidden once player hides them (button) - player be re-show any time
}
