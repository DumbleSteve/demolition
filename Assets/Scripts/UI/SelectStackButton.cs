using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


// toggle button representing a stack
// instantiated as each stack is built

[RequireComponent(typeof(Toggle))]

public class SelectStackButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI stackName;
    [SerializeField] private TextMeshProUGUI playerName;
    [SerializeField] private Image healthPanel;                     // current strength / initial strength of all blocks
    [SerializeField] private Slider healthBar;                      // overall stack health as a slider
    [SerializeField] private TextMeshProUGUI stackHealthText;       // overall stack health as a %

    [SerializeField] private Image fallenPanel;                     // fallen + demolished blocks
    [SerializeField] private Slider fallenBar;   

    [SerializeField] private Image currentPlayerLeft;
    [SerializeField] private Image currentPlayerRight;
    [SerializeField] private TextMeshProUGUI extraHitsText;
    [SerializeField] private TextMeshProUGUI demolishedText;
    [SerializeField] private TextMeshProUGUI rankText;
    [SerializeField] private Image characterSprite;                 // sprite from lobby

    [SerializeField] private Color onColour;                        // background
    [SerializeField] private Color offColour;                       // background
    [SerializeField] private Color maxChargeChangedColour = Color.yellow;
    [SerializeField] private Color stackDemolishedColour = Color.red;       // stack's player 'out'
    [SerializeField] private Color nameDemolishedColour = Color.grey;       // stack name text

    [SerializeField] private Color frozenColour = Color.grey;       // background
    [SerializeField] private Color nameFrozenColour = Color.cyan;

    private float sliderUpdateTime = 0.2f;
    private float chargeChangedTime = 0.3f;
    private float chargeChangedPause = 0.5f;

    private Color characterDisabledColor = new(1, 1, 1, 0.15f);
    private float spritePulseFactor = 1.4f;
    private float spritePulseTime = 0.5f;
    private Vector3 spriteScale;

    private bool gameOver;

    private Toggle button;
    private Stack buttonStack = null;           // stack (therefore StackPlayer) that this button is linked to
    private Color playerColour;                 // colour of player assigned to buttonStack

    private Image background;
    private Pulser pulser;


    private void Awake()
    {
        button = GetComponent<Toggle>();
        background = GetComponent<Image>();
        pulser = GetComponent<Pulser>();

        spriteScale = characterSprite.transform.localScale;
    }

    private void OnEnable()
    {
        button.onValueChanged.AddListener(SelectStack);                 // camera orbits buttonStack      // TODO: reinstate manual stack selection?

        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;                // first stack is selected once all built
        GameEvents.OnAllPlayersLinked += OnAllPlayersLinked;            // all NetCodePlayers and StackPlayers linked up
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;            // current stack changed
        GameEvents.OnStackDamaged += OnStackDamaged;                    // whenever a block is damaged
        GameEvents.OnFreezeStack += OnFreezeStack;                      // via rune - until player's turn again
        GameEvents.OnTurnChanged += OnTurnChanged;                      // on turn start
        GameEvents.OnMaxChargeChanged += OnMaxChargeChanged;            // for player (by rune)
        GameEvents.OnExtraHitsChanged += OnExtraHitsChanged;            // for player (by rune)
        GameEvents.OnStackFallenBlocks += OnStackFallenBlocks;          // when block demolished or falls
        GameEvents.OnStackDemolished += OnStackDemolished;              // block demolished threshold reached
        GameEvents.OnStackRank += OnStackRank;                          // after stack demolished
        GameEvents.OnGameOver += OnGameOver;                            // all stacks demolished
    }

    private void OnDisable()
    {
        button.onValueChanged.RemoveListener(SelectStack);

        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnAllPlayersLinked -= OnAllPlayersLinked;
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack; 
        GameEvents.OnStackDamaged -= OnStackDamaged;
        GameEvents.OnFreezeStack -= OnFreezeStack;
        GameEvents.OnTurnChanged -= OnTurnChanged;
        GameEvents.OnMaxChargeChanged -= OnMaxChargeChanged;  
        GameEvents.OnExtraHitsChanged -= OnExtraHitsChanged;
        GameEvents.OnStackFallenBlocks -= OnStackFallenBlocks;
        GameEvents.OnStackDemolished -= OnStackDemolished;
        GameEvents.OnStackRank -= OnStackRank;
        GameEvents.OnGameOver -= OnGameOver;
    }

    private void Start()
    {
        extraHitsText.text = "";
        rankText.text = "";
        demolishedText.gameObject.SetActive(false);

        EnableCurrentPlayerFlags(false);
    }

    // first stack is selected once all built
    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        button.interactable = true;
    }

    // initialise
    public void Init(Stack stack, ToggleGroup group, Color playerColour)
    {
        buttonStack = stack;
        this.playerColour = playerColour;

        stackName.text = name = stack.StackName;
        stackName.color = playerColour;
        playerName.text = stack.PlayerName;
        playerName.color = playerColour;
        rankText.color = playerColour;

        button.group = group;               // toggle group
        button.interactable = false;        // until all stacks built

        healthBar.minValue = 0f;
        healthBar.maxValue = 1f;

        fallenPanel.gameObject.SetActive(true);
        healthPanel.gameObject.SetActive(false);

        demolishedText.gameObject.SetActive(false);
        stackHealthText.gameObject.SetActive(false);

        characterSprite.gameObject.SetActive(false);      // enabled when sprite set

        // get stack health to set initial health bar value
        stack.StackDamaged();       // calculates total stack strength and fires OnStackDamaged event

        if (pulser != null)
            pulser.Pulse(transform);
    }

    public void SetPlayerName()
    {
        playerName.text = buttonStack.StackPlayer.PlayerName;
    }

    // set player character sprite
    private void OnAllPlayersLinked()
    {
        characterSprite.sprite = buttonStack.StackPlayer.Data.LobbyPlayerData.PlayerCharacter;    // LobbyPlayerData set when linked to NetcodePlayer
        characterSprite.gameObject.SetActive(true);
    }

    // on button clicked
    private void SelectStack(bool selected)
    {
        if (gameOver)
            return;

        SetColour();

        if (selected)
            GameEvents.OnManualStackSelect?.Invoke(buttonStack);
    }

    private void SetColour()
    {
        if (gameOver)
            background.color = stackDemolishedColour;
        else if (button.isOn)
            background.color = onColour;
        else if (buttonStack.StackDemolished)
            background.color = stackDemolishedColour;
        else if (buttonStack.StackFrozen)
            background.color = frozenColour;
        else
            background.color = offColour;
    }

    // current stack changed
    private void OnCameraOrbitStack(Stack stack)
    {
        if (buttonStack == stack)
            button.isOn = true;         // others will toggle off
    }

    // update health bar whenever a block in this stack is damaged
    private void OnStackDamaged(Stack stack, float initialStrength, float currentStrength)
    {
        if (stack != buttonStack)
            return;

        float newHealth = currentStrength / initialStrength;
        int damagedPercent = (int) Mathf.Round((1f - newHealth) * 100f);
        stackHealthText.text = $"{damagedPercent}%";

        LeanTween.value(healthBar.gameObject, healthBar.value, newHealth, sliderUpdateTime)
                            .setEaseInOutSine()
                            .setOnUpdate((float f) => healthBar.value = f);
    }

    private void OnFreezeStack(Stack stack, bool freeze)
    {
        if (buttonStack != stack)
            return;

        SetColour();
        stackName.color = (freeze ? nameFrozenColour : playerColour);

        if (freeze && pulser != null)
            pulser.Pulse(transform);
    }

    // fired OnTurnStart, before next player set
    private void OnStackFallenBlocks(Stack stack, float fallenPercent)
    {
        if (buttonStack != stack)      // not this button!
            return;

        LeanTween.value(fallenBar.gameObject, fallenBar.value, fallenPercent, sliderUpdateTime)
                    .setEaseInOutSine()
                    .setOnUpdate((float f) => fallenBar.value = f);
    }

    private void OnStackDemolished(Stack stack)
    {
        if (buttonStack != stack)
            return;

        SetColour();

        demolishedText.gameObject.SetActive(true);
        stackHealthText.gameObject.SetActive(true);
        stackName.color = nameDemolishedColour;
        fallenPanel.gameObject.SetActive(false);
        //healthPanel.gameObject.SetActive(true);
        extraHitsText.text = "";

        fallenBar.value = 0f;       // might not be exactly synced over network (physics differences)
        healthBar.value = 0f;       // loose end

        if (pulser != null)
            pulser.Pulse(transform);
    }

    // immediately after stack demolished
    private void OnStackRank(Stack stack, int rank)
    {
        if (buttonStack != stack)
            return;

        rankText.text = rank switch
        {
            1 => "1st",
            2 => "2nd",
            3 => "3rd",
            4 => "4th",
            _ => "",
        };

        if (pulser != null)
            pulser.Pulse(rankText.transform);
    }

    private void OnMaxChargeChanged(Stack stack, int maxCharge)
    {
        if (buttonStack != stack)
            return;

        if (buttonStack.StackDemolished)
            return;

        var currentColour = background.color;

        if (maxCharge > 0)
        {
            LeanTween.value(gameObject, currentColour, maxChargeChangedColour, chargeChangedTime)
                        .setEaseOutCirc()
                        .setOnUpdate((Color c) => background.color = c)
                        .setOnComplete(() =>
                        {
                            LeanTween.value(gameObject, maxChargeChangedColour, currentColour, chargeChangedTime)
                                        .setDelay(chargeChangedPause)
                                        .setEaseInCirc()
                                        .setOnUpdate((Color c) => background.color = c);
                        });
        }
        else    // no charge left!
        {
            LeanTween.value(gameObject, currentColour, stackDemolishedColour, chargeChangedTime)
                        .setEaseOutCirc()
                        .setOnUpdate((Color c) => background.color = c);
        }

        if (pulser != null)
            pulser.Pulse(transform);
    }

    private void OnExtraHitsChanged(Stack stack, int extraHits)
    {
        if (buttonStack != stack)
            return;

        //string hitsLabel = extraHits == 1 ? $"+ {extraHits} Hit" : $"+ {extraHits} Hits";
        //extraHitsText.text = $"{(extraHits > 0 ? hitsLabel : "")}";

        string hitsLabel = (extraHits > 0) ? ("+" + extraHits) : "";
        extraHitsText.text = hitsLabel;

        if (extraHits > 0 && pulser != null)
            pulser.Pulse(extraHitsText.transform);
    }

    private void OnTurnChanged(StackPlayer newTurnPlayer)
    {
        EnableCurrentPlayerFlags(newTurnPlayer.PlayerStack == buttonStack);
        //EnableCurrentPlayerFlags(newTurnPlayerId == buttonStack.StackPlayer.NetcodePlayerId);
    }

    private void EnableCurrentPlayerFlags(bool enable)
    {
        //currentPlayerLeft.enabled = enable;
        currentPlayerRight.enabled = enable;

        if (enable && pulser != null)
            pulser.Pulse(transform);

        characterSprite.color = enable ? Color.white : characterDisabledColor;

        LeanTween.scale(characterSprite.gameObject, enable ? spriteScale * spritePulseFactor : spriteScale, spritePulseTime)
                            .setEaseOutBack();
    }

    private void OnGameOver(StackPlayer winner)
    {
        EnableCurrentPlayerFlags(false);
        gameOver = true;
        SetColour();
    }
}
