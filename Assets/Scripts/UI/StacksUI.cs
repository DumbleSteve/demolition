using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ToggleGroup))]

public class StacksUI : MonoBehaviour
{
    [SerializeField] private SelectStackButton SelectStackButtonPrefab;

    private ToggleGroup toggleGroup;

    private void Awake()
    {
        toggleGroup = GetComponent<ToggleGroup>();       // required
    }

    private void OnEnable()
    {
        GameEvents.OnStackPlayerSet += OnStackPlayerSet;        // spawn stack button
        GameEvents.OnDestroyStacks += OnDestroyStacks;          // clear stack buttons
    }

    private void OnDisable()
    {
        GameEvents.OnStackPlayerSet -= OnStackPlayerSet;
        GameEvents.OnDestroyStacks -= OnDestroyStacks;
    }

    // create a new button for the stack just built
    // once a player has been assigned to it
    private void OnStackPlayerSet(StackPlayer stackPlayer, Stack stack, Color playerColour)
    {
        SelectStackButton stackButton = Instantiate(SelectStackButtonPrefab, transform);     // vertical layout group
        stackButton.Init(stack, toggleGroup, playerColour);
    }

    // clear stack buttons
    private void OnDestroyStacks()
    {
        foreach (Transform stackButton in transform)
        {
            Destroy(stackButton.gameObject);
        }
    }
}
