using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;


public class RunesUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI runeText;

    private const float timedOutTextTime = 3f;            // seconds

    private Pulser pulser;      // optional

    private void OnEnable()
    {
        //GameEvents.OnRuneRevealed += OnRuneRevealed;
        GameEvents.OnRuneActivated += OnRuneActivated;
        //GameEvents.OnRuneExpired += OnRuneExpired;
    }

    private void OnDisable()
    {
    //    GameEvents.OnRuneRevealed -= OnRuneRevealed;
        GameEvents.OnRuneActivated -= OnRuneActivated;
        //GameEvents.OnRuneExpired -= OnRuneExpired;
    }

    private void Start()
    {
        runeText.text = "";
        pulser = GetComponent<Pulser>();
    }

    //private async void OnRuneRevealed(Rune rune)
    //{
    //    //runeText.text = $"{rune.Data.RevealText}\n{RuneData.PowerLabel(rune.Data.Power)}";
    //    runeText.text = $"{RuneData.PowerLabel(rune.Data.Power)} Rune Revealed!";

    //    if (pulser != null)
    //        pulser.Pulse(runeText.transform);

    //    await Task.Delay((int)(timedOutTextTime * 1000f));
    //    runeText.text = "";
    //}

    private async void OnRuneActivated(Rune rune)
    {
        RuneData.GetPowerLabel(rune.Data.Power, out string runePower, out string runeDescription);
        runeText.text = $"'{runePower}' Rune Activated!";

        if (pulser != null)
            pulser.Pulse(runeText.transform);

        await Task.Delay((int)(timedOutTextTime * 1000f));
        runeText.text = "";
    }

    //private async void OnRuneExpired(Rune rune)
    //{
    //    //runeText.text = $"{rune.Data.ExpiryText}\n[ {RuneData.PowerLabel(rune.Data.Power)} ]";
    //    runeText.text = $"{RuneData.PowerLabel(rune.Data.Power)} Rune Expired!";

    //    await Task.Delay((int)(timedOutTextTime * 1000f));
    //    runeText.text = "";
    //}
}
