using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintsUI : MonoBehaviour
{
    [SerializeField] private PlayerDataSO PlayerData;       // saved player data

    [SerializeField] private Image PlayersPanel;
    [SerializeField] private Image StackPanel;
    [SerializeField] private Image MouseWheelPanel;
    [SerializeField] private Image DetonatePanel;
    [SerializeField] private Image RunesPanel;
    [SerializeField] private Image RaycastPanel;

    [SerializeField] private Button HintsButton;
    //[SerializeField] private Image HintsDisabled;

    [SerializeField] private AudioClip ShowAudio;

    private bool hintsShowing = false;
    private float panelShowTime = 0.4f;

    private List<Image> panelsShowing = new();


    private void OnEnable()
    {
        GameEvents.OnStackPlayerSet += OnStackPlayerSet;
        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;
        GameEvents.OnRaycastHit += OnRaycastHit;
        GameEvents.OnDetonate += OnDetonate;

        HintsButton.onClick.AddListener(OnHintsButtonClicked);
    }

    private void OnDisable()
    {
        GameEvents.OnStackPlayerSet -= OnStackPlayerSet;
        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnRaycastHit -= OnRaycastHit;
        GameEvents.OnDetonate -= OnDetonate;

        HintsButton.onClick.RemoveListener(OnHintsButtonClicked);
    }

    private void Awake()
    {
        PlayersPanel.gameObject.SetActive(false);
        StackPanel.gameObject.SetActive(false);
        MouseWheelPanel.gameObject.SetActive(false);
        DetonatePanel.gameObject.SetActive(false);
        RunesPanel.gameObject.SetActive(false);
        RaycastPanel.gameObject.SetActive(false);

        HintsButton.gameObject.SetActive(false);
    }

    private void Start()
    {
        ShowAll(false);
        hintsShowing = true;
        //HintsDisabled.enabled = PlayerData.HintsHidden;
    }

    private void OnStackPlayerSet(StackPlayer stackPlayer, Stack stack, Color playerColour)
    {
        //ShowAll(false);
        HintsButton.gameObject.SetActive(true);

        // when first player has been assigned a stack, show players panel
        if (!PlayerData.HintsHidden && stackPlayer.Data.PlayerIndex == 0)
        {
            hintsShowing = true;
            ShowPanel(PlayersPanel, true);
        }
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        if (PlayerData.HintsHidden || !hintsShowing)
            return;

        ShowPanel(PlayersPanel, false);

        ShowPanel(DetonatePanel, true);
        ShowPanel(StackPanel, true);
    }

    private void OnRaycastHit(List<StackBlock> hitBlocks, Rune hitRune)
    {
        if (hitBlocks == null && hitRune == null)
            return;

        if (PlayerData.HintsHidden || !hintsShowing)
            return;

        ShowPanel(StackPanel, false);
        ShowPanel(MouseWheelPanel, true);
        ShowPanel(RunesPanel, true);
        //ShowPanel(RaycastPanel, true);
    }

    private void OnDetonate(float damagePerBlock, Rune rune)
    {
        ShowAll(false);
    }

    private void OnHintsButtonClicked()
    {
        // toggle hints
        hintsShowing = !hintsShowing;
        ShowAll(hintsShowing);

        if (!PlayerData.HintsHidden && !hintsShowing)
            PlayerData.HintsHidden = true;        // don't auto-show again once user has hidden hints via button

        //HintsDisabled.enabled = !hintsShowing;
    }

    private void ShowPanel(Image panel, bool show)
    {
        Vector3 targetScale = show ? Vector2.one : Vector2.zero;
        LeanTweenType easing = show ? LeanTweenType.easeOutBack : LeanTweenType.easeInQuart;

        if (show && panelsShowing.Contains(panel))      // already showing
            return;

        if (!show && !panelsShowing.Contains(panel))    // already not showing
            return;

        if (show)
        {
            panel.transform.localScale = Vector2.zero;
            panel.gameObject.SetActive(true);

            panelsShowing.Add(panel);
        }
        else
        {
            panelsShowing.Remove(panel);
        }

        LeanTween.scale(panel.gameObject, targetScale, panelShowTime)
                    .setEase(easing);

        if (ShowAudio != null)
            AudioSource.PlayClipAtPoint(ShowAudio, Vector3.zero);
    }

    public void ShowAll(bool show)
    {
        ShowPanel(PlayersPanel, show);
        ShowPanel(StackPanel, show);
        ShowPanel(MouseWheelPanel, show);
        ShowPanel(DetonatePanel, show);
        ShowPanel(RunesPanel, show);
        //ShowPanel(RaycastPanel, show);
    }
}
