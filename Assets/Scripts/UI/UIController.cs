using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

// handles all button interaction
// creates a toggle button for each stack as it is built

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI CurrentPlayer;
    [SerializeField] private TextMeshProUGUI StatusText;
    [SerializeField] private TextMeshProUGUI TurnText;
    [SerializeField] private Pulser TurnTextPulser;

    [SerializeField] private PlayersUI PlayersPanel;
    [SerializeField] private DamageUI DamagePanel;
    [SerializeField] private RaycastUI RaycastPanel;
    [SerializeField] private RunesUI RunesPanel;

    [SerializeField] private Image GameOverPanel;
    [SerializeField] private Button PlayAgainButton;       // reload scene

    [SerializeField] private MusicMuteUI MusicMuteUI;      // enabled OnLobbyStartGame

    [SerializeField] private TextMeshProUGUI debugText;

    private const float timedOutStatusTime = 2f;            // seconds

    private bool localPlayersTurn = false;
    private const float yourTurnPulseInterval = 2.5f;       // seconds
    private bool yourTurnPulsing = false;                   // don't start pulse again if same player's turn (eg. 1 player game or last stack standing)

    private string currentPlayerName;
    private Color currentPlayerColour;

    private Pulser pulser;                                  // may be null!


    private void Awake()
    {
        pulser = GetComponent<Pulser>();

        CurrentPlayer.text = "";
        StatusText.text = "";
        TurnText.text = "";

        ShowAll(false);         // shown on game started (via lobby)

        MusicMuteUI.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        PlayAgainButton.onClick.AddListener(PlayAgain);

        GameEvents.OnLobbyStartGame += OnLobbyStartGame;                      // via lobby

        GameEvents.OnBuildStacks += OnBuildStacks;               
        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;

        GameEvents.OnTurnChanged += OnTurnChanged;
        GameEvents.OnTurnEnd += OnTurnEnd;                      // stack detonated or charge timed out
        GameEvents.OnGameOver += OnGameOver;                      // all players 'out'

        GameEvents.OnIsLocalPlayersTurn += OnIsLocalPlayersTurn;            // determined by NetcodePlayer and broadcast to all players via ClienRPC
    }

    private void OnDisable()
    {
        PlayAgainButton.onClick.RemoveListener(PlayAgain);

        GameEvents.OnLobbyStartGame -= OnLobbyStartGame;

        GameEvents.OnBuildStacks -= OnBuildStacks;
        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;

        GameEvents.OnTurnChanged -= OnTurnChanged;
        GameEvents.OnTurnEnd -= OnTurnEnd;
        GameEvents.OnGameOver -= OnGameOver;

        GameEvents.OnIsLocalPlayersTurn -= OnIsLocalPlayersTurn;
    }

    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        ShowAll(true);
        StatusText.text = "Connecting Players...";

        MusicMuteUI.gameObject.SetActive(true);
    }

    private void OnBuildStacks(List<BlockData> blockList, StackData stackData)
    {
        StatusText.text = "Building Stacks...";
        CurrentPlayer.text = "";
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        StatusText.text = "";
    }

    private void OnTurnChanged(StackPlayer currentPlayer)
    {
        currentPlayerName = currentPlayer.PlayerName;
        currentPlayerColour = currentPlayer.PlayerColour;

        CurrentPlayer.text = currentPlayer != null ? currentPlayerName : "";
        CurrentPlayer.color = currentPlayerColour;

        if (pulser != null)
            pulser.Pulse(CurrentPlayer.transform);
    }

    private void OnIsLocalPlayersTurn(bool isLocalPlayersTurn, int numPlayers)
    {
        localPlayersTurn = isLocalPlayersTurn;

        //TurnText.text = isLocalPlayersTurn ? "Your Turn!" : $"{currentPlayerName}'s Turn";        // TODO: currentPlayerName not set for first turn?
        TurnText.text = isLocalPlayersTurn && numPlayers > 1 ? "Your Turn!" : "";
        TurnText.color = currentPlayerColour;

        if (isLocalPlayersTurn && numPlayers > 1 && !yourTurnPulsing)        // don't start coroutine again if same player's turn (eg. 1 player game or last stack standing)
            StartCoroutine(PulseYourTurn());
    }

    private IEnumerator PulseYourTurn()
    {
        if (TurnTextPulser == null || yourTurnPulsing)
            yield break;

        yourTurnPulsing = true;

        while (localPlayersTurn)
        {
            TurnTextPulser.Pulse(TurnText.transform);
            yield return new WaitForSeconds(yourTurnPulseInterval);
        }

        yourTurnPulsing = false;
    }

    private async void OnTurnEnd(StackPlayer player, bool timedOut)
    {
        if (timedOut)
        {
            StatusText.text = "Time's Up!";
            if (pulser != null)
                pulser.Pulse(StatusText.transform);

            await Task.Delay((int)(timedOutStatusTime * 1000f));
            StatusText.text = "";
        }
    }

    private void OnGameOver(StackPlayer winner)
    {
        GameOverPanel.gameObject.SetActive(true);
        DamagePanel.gameObject.SetActive(false);
        RaycastPanel.gameObject.SetActive(false);
        RunesPanel.gameObject.SetActive(false);
        StatusText.text = $"{winner.PlayerName} Wins!!";

        GameEvents.OnCameraOrbitTable?.Invoke();
    }

    private void ShowAll(bool show)
    {
        GameOverPanel.gameObject.SetActive(false);  // only show on game over!

        PlayersPanel.gameObject.SetActive(show);
        DamagePanel.gameObject.SetActive(show);
        RaycastPanel.gameObject.SetActive(show);
        RunesPanel.gameObject.SetActive(show);
        StatusText.text = "";
    }

    private void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
