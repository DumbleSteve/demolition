using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// UI panel to show details of selected blocks or rune,
// as a reult of raycasting

public class RaycastUI : MonoBehaviour
{
    [SerializeField] private Image raycastPanel;

    [SerializeField] private TextMeshProUGUI headerText;

    [Header("Selected Blocks")]
    [SerializeField] private Image blockPanel;
    [SerializeField] private TextMeshProUGUI blockCountText;        // hit blocks
    [SerializeField] private TextMeshProUGUI blockDepthText;        // depth as per mouse wheel delta
    [SerializeField] private TextMeshProUGUI damagePerBlockText;    // blocks / charge remaining
    [SerializeField] private TextMeshProUGUI currentHealthText;     // of hit blocks
    [SerializeField] private TextMeshProUGUI damagedHealthText;     // of hit blocks

    [Header("Selected Rune")]
    [SerializeField] private Image runePanel;
    [SerializeField] private TextMeshProUGUI runePowerText;
    [SerializeField] private TextMeshProUGUI runeDescriptionText;
    [SerializeField] private TextMeshProUGUI runeValueText;
    [SerializeField] private TextMeshProUGUI runeExpiryText;
    [SerializeField] private TextMeshProUGUI runeMinChargeText;

    [SerializeField] private Color plusColour = Color.green;        // charge / strength runes
    [SerializeField] private Color minusColour = Color.red;         // charge / strength runes

    private Vector2 panelStartScale;
    private float panelScaleTime = 0.3f;
    private bool panelVisible = false;

    private int hitBlockCount = 0;
    private float totalInitialStrength = 0;
    private float totalCurrentStrength = 0;
    private float currentHealthPercent = 0;
    private float chargeRemaining = 0;
    private float strengthRemaining = 0;
    private int blockSelectDepth = 1;           // as per mouse wheel delta

    private float DamagePerBlock => (blockSelectDepth > 0 && chargeRemaining > 0) ? (chargeRemaining / blockSelectDepth) : 0;

    private Pulser pulser;


    private void Awake()
    {
        pulser = GetComponent<Pulser>();

        panelStartScale = raycastPanel.transform.localScale;

        panelVisible = false;
        raycastPanel.transform.localScale = new Vector2(panelStartScale.x, 0f);
        raycastPanel.gameObject.SetActive(true);

        damagedHealthText.text = "";      // set on charge tick

        blockPanel.gameObject.SetActive(false);
        runePanel.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        GameEvents.OnRaycastHit += OnRaycastHit;                        // multiple blocks or single rune
        GameEvents.OnTurnChargeTick += OnTurnChargeTick;                // charge countdown
        GameEvents.OnBlockHitDepthChanged += OnBlockHitDepthChanged;    // from raycast / mouse wheel

        // init text value when enabled (changes only on mouse wheel delta)
        SetBlockDepth(blockSelectDepth, true);
    }

    private void OnDisable()
    {
        GameEvents.OnRaycastHit -= OnRaycastHit;
        GameEvents.OnTurnChargeTick -= OnTurnChargeTick;
        GameEvents.OnBlockHitDepthChanged -= OnBlockHitDepthChanged;
    }

    private void OnRaycastHit(List<StackBlock> hitBlocks, Rune hitRune)
    {
        if (hitBlocks != null)
            hitBlockCount = hitBlocks.Count;

        bool hasHitBlocks = (hitBlocks != null && hitBlockCount > 0);
        if (hasHitBlocks && hitBlocks[0].ParentStack.StackDemolished || (hitRune != null && hitRune.ParentStack.StackDemolished))
        {
            ShowDataPanel(false);
            return;
        }

        ShowDataPanel(hasHitBlocks || hitRune != null);

        if (hitBlocks != null && hitBlockCount > 0)            // block data showing
        {
            totalInitialStrength = 0;
            totalCurrentStrength = 0;
            currentHealthPercent = 0;

            foreach (var block in hitBlocks)
            {
                totalInitialStrength += block.BlockData.initialStrength;
                totalCurrentStrength += block.BlockData.currentStrength;
            }

            currentHealthPercent = totalInitialStrength > 0 ? totalCurrentStrength / totalInitialStrength : 0;
            currentHealthPercent *= 100f;

            EnableBlockRuneText(true);

            headerText.text = "Blocks to Detonate";
            blockCountText.text = $"{hitBlockCount} {(hitBlockCount == 1 ? "Block" : "Blocks")}";
            damagePerBlockText.text = blockSelectDepth > 1 ? $"{(int)DamagePerBlock} per Block" : "";

            currentHealthText.text = $"{(int)totalCurrentStrength} / {(int)totalInitialStrength} : {(int)currentHealthPercent}%";
        }
        else if (hitRune != null)               // rune data showing
        {
            var runeData = hitRune.Data;

            EnableBlockRuneText(false);

            headerText.text = "Rune to Activate";

            RuneData.GetPowerLabel(hitRune.Data.Power, out string runePower, out string runeDescription);

            runePowerText.text = $"{runePower}!";
            runePowerText.color = hitRune.Data.RuneTextColour;

            runeDescriptionText.text = $"{runeDescription}";

            string valueName = "";

            switch (hitRune.Data.Power)
            {
                case RuneData.RunePower.Nothing:
                    break;
                case RuneData.RunePower.PlayerCharge:
                    valueName = "Charge Boost:";
                    break;
                case RuneData.RunePower.StackStrength:
                    valueName = "Strength Boost:";
                    break;
                case RuneData.RunePower.FreezeStack:
                    break;
                case RuneData.RunePower.StackRestore:
                    break;
                case RuneData.RunePower.ExtraHit:
                    valueName = "Extra Hits:";
                    break;
            }

            runeValueText.text = valueName != "" ? $"{valueName}  {hitRune.RuneValue}" : "";
            runeValueText.color = hitRune.RuneValue >= 0 ? plusColour : minusColour;

            //runeMinChargeText.text = hitRune.RuneMinCharge > 0 ? $"Activation Charge:  {hitRune.RuneMinCharge}" : "";
            runeMinChargeText.text = $"Activation Charge:  {(hitRune.RuneMinCharge > 0 ? hitRune.RuneMinCharge : "--")}";
            runeExpiryText.text = runeData.ExpiryTime > 0 ? $"Expiry Time:  {runeData.ExpiryTime}" : "";
        }
    }

    private void OnTurnChargeTick(int maxDamageCharge, int damageChargeRemaining, bool turnEnded)
    {
        if (turnEnded)
            return;

        chargeRemaining = damageChargeRemaining;

        strengthRemaining  = totalCurrentStrength - chargeRemaining;
        if (strengthRemaining < 0)
            strengthRemaining = 0;      // all hit blocks demolished..!?

        if (panelVisible)
        {
            float damagedHealthPercent = totalInitialStrength > 0 ? strengthRemaining / totalInitialStrength : 0f;
            damagedHealthPercent *= 100f;

            damagedHealthText.text = $"{(int)totalInitialStrength} / {(int)strengthRemaining} : {(int)damagedHealthPercent}%";
        }
    }

    // mouse wheel delta
    private void OnBlockHitDepthChanged(int blocksToDamage)
    {
        SetBlockDepth(blocksToDamage, false);
    }

    private void SetBlockDepth(int blocksToDamage, bool init)
    {
        if (blockSelectDepth == blocksToDamage && !init)
            return;

        blockSelectDepth = blocksToDamage;
        blockDepthText.text = $"{blocksToDamage} Deep";

        if (pulser != null && !init)
            pulser.Pulse(blockDepthText.transform);
    }

    private void EnableBlockRuneText(bool blocks)
    {
        blockPanel.gameObject.SetActive(blocks);
        runePanel.gameObject.SetActive(!blocks);
    }

    // roll panel up (pivot is bottom centre)
    private void ShowDataPanel(bool show)
    {
        if (panelVisible == show)       // no change, do nothing
           return;

        if (show)
        {
            raycastPanel.transform.localScale = new Vector2(panelStartScale.x, 0f);
            panelVisible = true;

            LeanTween.scaleY(raycastPanel.gameObject, panelStartScale.y, panelScaleTime)
                        .setEaseOutBack();
        }
        else    // hide
        {
            LeanTween.scaleY(raycastPanel.gameObject, 0f, panelScaleTime / 2f)
                        .setEaseInQuart()
                        .setOnComplete(() => { panelVisible = false; });
        }
    }
}
