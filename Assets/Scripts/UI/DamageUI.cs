using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// UI for block / rune 'detonation'

public class DamageUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI damageChargeText;          // total amount of damage available to deliver (counts down)
    [SerializeField] private Slider chargeSlider;
    [SerializeField] private Detonator detonator;

    [SerializeField] private TextMeshProUGUI turnEndsText;              // countdown after detonation
    [SerializeField] private Button endTurnButton;                     // 'end turn' enabled OnTurnEnd - starts next player's turn

    [SerializeField] private Color turnInProgressColour = Color.white;
    //[SerializeField] private Color turnEndedColour = Color.yellow;
    [SerializeField] private Color chargeWarningColour = Color.red;

    private const float chargeWarningPercent = 0.1f;                     // % of maxCharge - damageChargeText countdown changes to chargeWarningColour
    private int chargeWarningLevel => (int)(maxCharge * chargeWarningPercent);     // % of maxCharge - damageChargeText countdown changes to chargeWarningColour

    private int blockSelectDepth = 1;                                    // as per mouse wheel
    private float maxCharge;
    private float chargeRemaining;                                      // counts down

    private float chargeSliderUpdateTime = 0.25f;                       // only when refilled

    private List<StackBlock> raycastBlocks;                             // set on raycast hit
    private Rune raycastRune = null;                                    // set on raycast hit

    private Pulser pulser;                                              // may be null!
    private Stack currentStack = null;                                  // 'in focus'

    private bool turnEnded = false;
    private bool detonated;

    private void Awake()
    {
        pulser = GetComponent<Pulser>();
        detonator.PrimeDetonator(DetonateClicked, currentStack);
    }

    private void OnEnable()
    {
        endTurnButton.onClick.AddListener(NextTurnClicked);

        GameEvents.OnTurnChargeTick += OnTurnChargeTick;          // on acquired / spent charge

        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable += OnCameraOrbitTable;    // look at / follow table

        GameEvents.OnBlockHitDepthChanged += OnBlockHitDepthChanged;      // raycast / mouse wheel

        GameEvents.OnRaycastHit += OnRaycastHit;
        GameEvents.OnStackDemolished += OnStackDemolished;
        GameEvents.OnTurnStart += OnTurnStart;
        GameEvents.OnTurnEnd += OnTurnEnd;
    }

    private void OnDisable()
    {
        endTurnButton.onClick.RemoveListener(NextTurnClicked);

        GameEvents.OnTurnChargeTick -= OnTurnChargeTick;

        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable -= OnCameraOrbitTable;

        GameEvents.OnBlockHitDepthChanged -= OnBlockHitDepthChanged;

        GameEvents.OnRaycastHit -= OnRaycastHit;
        GameEvents.OnStackDemolished -= OnStackDemolished;
        GameEvents.OnTurnStart -= OnTurnStart;
        GameEvents.OnTurnEnd -= OnTurnEnd;
    }

    private void Start()
    {
        DisableAllButtons();
        damageChargeText.text = $"Charge: ---";
        turnEndsText.text = "";
    }

    private void OnTurnChargeTick(int maxDamageCharge, int damageChargeRemaining, bool turnEnded)
    {
        maxCharge = maxDamageCharge;
        chargeRemaining = damageChargeRemaining;
        this.turnEnded = turnEnded;

        EnableDetonate();       // if sufficent charge remaining (for raycast rune)

        if (maxCharge > 0)
        {
            float sliderValue = chargeRemaining / maxCharge;

            if (chargeSlider.value != sliderValue)
            {
                if (sliderValue == 1f)        // fill up slowly for effect
                {
                    LeanTween.value(chargeSlider.gameObject, chargeSlider.value, sliderValue, chargeSliderUpdateTime)
                                        .setEaseInOutSine()
                                        .setOnUpdate((float f) => chargeSlider.value = f);
                }
                else        // update immediately (small increment)
                {
                    chargeSlider.value = sliderValue;
                }
            }
        }

        if (!turnEnded)
        {
            damageChargeText.text = $"Charge:  {(chargeRemaining > 0 ? chargeRemaining : "--")}";
            damageChargeText.color = (chargeRemaining > 0 && chargeRemaining <= chargeWarningLevel) ? chargeWarningColour : turnInProgressColour;
            turnEndsText.text = "";
        }
        else
        {
            turnEndsText.text = $"Turn Ends\nin {chargeRemaining}";
            damageChargeText.text = "";
            //damageChargeText.color = turnEndedColour;
        }
    }

    private void OnBlockHitDepthChanged(int blocksToDamage)
    {
        if (blocksToDamage > 0)
            blockSelectDepth = blocksToDamage;
    }

    // detonate blocks selected by raycast / mouse wheel
    private void DetonateClicked()
    {
        if (!NetworkInterface.Instance.IsLocalPlayersTurn)            // to make sure!
            return;

        if (chargeRemaining > 0)
        {
            NetworkInterface.Instance.NetworkActivateRune();        // activate rune before OnDetonate, which determines if turn ends (extra hits or not)
   
            if (blockSelectDepth > 0)
                NetworkInterface.Instance.NetworkDetonateBlocks(chargeRemaining / blockSelectDepth);   // damage per block - fires OnTurnEnd if no extra hits
        }

        detonator.EnableDetonator(false);
        endTurnButton.interactable = true;
        detonated = true;       // to prevent next Update from reactivating detonator before turn ended (network latency?)
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        DisableAllButtons();
    }

    private void OnRaycastHit(List<StackBlock> hitBlocks, Rune hitRune)
    {
        raycastBlocks = hitBlocks;
        raycastRune = hitRune;

        EnableDetonate();
    }

    private void EnableDetonate()
    {
        // can only activate rune if it belongs to currentStack
        // and there is sufficicient charge remaining
        // and it hasn't already been activated
        if (NetworkInterface.Instance.IsLocalPlayersTurn && !turnEnded && !detonated)
        {
            bool canActivateRune = raycastRune != null && chargeRemaining >= raycastRune.RuneMinCharge;
            detonator.EnableDetonator(raycastBlocks != null || canActivateRune);
        }
        else
        {
            detonator.EnableDetonator(false);
        }
    }

    private void OnStackDemolished(Stack stack)
    {
        if (stack == currentStack)
        {
            detonator.EnableDetonator(false);
        }
    }

    private void OnCameraOrbitStack(Stack stack)
    {
        currentStack = stack;
    }

    private void OnCameraOrbitTable()
    {
        currentStack = null;
    }

    private void NextTurnClicked()
    {
        if (!NetworkInterface.Instance.IsLocalPlayersTurn)            // to make sure!
            return;

        endTurnButton.interactable = false;
        GameEvents.OnNextPlayerTurn?.Invoke();      // set next player and start turn for all players
    }

    private void OnTurnStart()
    {
        turnEnded = false;
        detonated = false;

        DisableAllButtons();
        chargeSlider.gameObject.SetActive(true);
    }

    private void OnTurnEnd(StackPlayer player, bool timedOut)
    {
        turnEnded = true;

        if (timedOut)
        {
            endTurnButton.interactable = false;
        }
        else
        {
            if (NetworkInterface.Instance.IsLocalPlayersTurn)
            {
                endTurnButton.interactable = true;
                pulser.Pulse(endTurnButton.transform);
            }
        }

        chargeSlider.gameObject.SetActive(false);
    }


    private void DisableAllButtons()
    {
        detonator.EnableDetonator(false);
        endTurnButton.interactable = false;
    }
}
