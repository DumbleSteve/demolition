using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// instantiated from prefab by TurnManager OnStackBuilt - always one player per stack

public class StackPlayer : MonoBehaviour
{
    [SerializeField] public PlayerData Data;                // visible in inspector

    public Stack PlayerStack { get; private set; }          // stack owned by this player
    public Color PlayerColour { get; private set; }         // defined in TurnManager

    public string PlayerName => Data.LobbyPlayerData.PlayerName; 
    public Sprite PlayerCharacter => Data.LobbyPlayerData.PlayerCharacter;

    public bool HasExtraHits => Data.ExtraHits > 0;

    // each NetcodePlayer will control one or more Stacks via a StackPlayer
    // so each StackPlayer will be controlled by a single NetcodePlayer (which may control >1 StackPlayers)
    // each Stack has one StackPlayer and vice versa
    public NetcodePlayer NetcodePlayer { get; private set; }
    public ulong NetcodePlayerId => NetcodePlayer.OwnerClientId;


    private void OnEnable()
    {
        GameEvents.OnChangeMaxCharge += OnChangeMaxCharge;
        GameEvents.OnChangeExtraHits += OnChangeExtraHits;
    }

    private void OnDisable()
    {
        GameEvents.OnChangeMaxCharge -= OnChangeMaxCharge;
        GameEvents.OnChangeExtraHits -= OnChangeExtraHits;
    }

    // assign player a stack and colour
    // on stack built
    public void AssignStack(Stack stack, int index, Color playerColour)
    {
        PlayerStack = stack;
        PlayerColour = playerColour;

        Data.PlayerIndex = index;

        GameEvents.OnStackPlayerSet?.Invoke(this, stack, PlayerColour);        // for this stack's SelectStackButton

        // event to initialise eg. SelectStackButton (in case player data saved at some point)
        GameEvents.OnExtraHitsChanged?.Invoke(PlayerStack, Data.ExtraHits);
    }

    // increase/decrease max charge if player owns this stack
    private void OnChangeMaxCharge(Stack stack, int chargeIncrement)
    {
        if (PlayerStack != stack)
            return;

        Data.MaxCharge += chargeIncrement;      // on player's next turn - may be negative!

        if (Data.MaxCharge <= 0)
        {
            Data.MaxCharge = 0;
            // TODO: player out of game event?
        }

        GameEvents.OnMaxChargeChanged?.Invoke(stack, Data.MaxCharge);
    }

    public void SetExtraHits(int extraHits)
    {
        if (Data.ExtraHits == extraHits)
            return;

        Data.ExtraHits = extraHits;

        // event to initialise eg. SelectStackButton (in case player data saved at some point)
        GameEvents.OnExtraHitsChanged?.Invoke(PlayerStack, Data.ExtraHits);
    }

    public void UseExtraHit()
    {
        if (Data.ExtraHits <= 0)
        {
            Data.ExtraHits = 0;
            return;
        }

        Data.ExtraHits--;
        GameEvents.OnExtraHitsChanged?.Invoke(PlayerStack, Data.ExtraHits);
    }

    // increase/decrease extra hits if player owns this stack
    private void OnChangeExtraHits(Stack stack, int hitsIncrement)
    {
        if (PlayerStack != stack)
            return;

        Data.ExtraHits += hitsIncrement;      // on player's next turn - hitsIncrement may be negative!

        if (Data.ExtraHits <= 0)
            Data.ExtraHits = 0;

        GameEvents.OnExtraHitsChanged?.Invoke(stack, Data.ExtraHits);
    }

    //// save player data from lobby
    //// called when all NetcodePlayers have been spawned and all Stacks built
    //public void SetLobbyPlayerData(LobbyPlayerData lobbyPlayerData)
    //{
    //    Data.LobbyPlayerData = lobbyPlayerData;
    //}

    // link this StackPlayer to its (single) NetcodePlayer
    // and set player name & character from lobby.
    // called when all NetcodePlayers have been spawned and all Stacks built
    public void AssignNetcodePlayer(NetcodePlayer netcodePlayer)
    {
        NetcodePlayer = netcodePlayer;
        Data.LobbyPlayerData = netcodePlayer.LobbyPlayerData;
    }
}
