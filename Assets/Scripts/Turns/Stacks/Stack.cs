using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEditor.PackageManager;


// comprises unlimited StackLevels, each comprising StackRows, each comprising StackBlocks

public class Stack : MonoBehaviour
{
    [SerializeField] private StackLevel stackLevelPrefab;
    public List<StackLevel> StackLevels { get; private set; } = new();
    //public float FallenBlockPercent => StackLevels.Average(level => level.FallenBlockPercent);
    //public int DemolishedBlocks => StackLevels.Sum(level => level.DemolishedBlocks);

    [SerializeField] private StackRunes stackRunes;             // set in inspector

    [SerializeField] private TextMeshPro stackTextFront;
    [SerializeField] private TextMeshPro stackTextBack;
    [SerializeField] private TextMeshPro stackTextTop;
    [SerializeField] private TextMeshPro stackPlayerName;

    [SerializeField] private Light spotLight;
    [SerializeField] private Light playerLight;                 // colour of StackPlayer
    [SerializeField] private AudioClip stackRestoreAudio;
    [SerializeField] private AudioClip stackDemolishedAudio;       // player 'out'
    [SerializeField] private Color demolishedLightColour = Color.red;

    [SerializeField] private AudioClip blockDropAudio;

    public Transform FocalPoint;                                // for orbit camera to look at

    public StackData StackBuildData { get; private set; }

    public StackPlayer StackPlayer { get; private set; }             // player that owns this stack
    public string StackName { get; private set; }
    public string PlayerName { get; private set; }

    private List<BlockData> orderedStackBlocks = new();      // sorted, ready to build levels
    private int StackBlockCount => orderedStackBlocks.Count;

    [SerializeField] private float stackDemolishedPercent = 0.7f;         // stack deemed 'demolished' when this percent of blocks have fallen/detonated

    public int SelectedBlockCount => orderedStackBlocks.Count(b => b.IsSelected);
    private int DemolishedBlockCount => orderedStackBlocks.Count(b => b.IsDemolished);
    private int FallenBlockCount => orderedStackBlocks.Count(b => b.IsFallen);
    private float FallenBlockPercent => Mathf.Clamp((float)(FallenBlockCount + DemolishedBlockCount) / ((float)StackDemolishedThreshold), 0f, 1f);

    private int StackDemolishedThreshold => (int) Mathf.Round(StackBlockCount * stackDemolishedPercent);   // number of blocks that must detonate/fall for stack to be 'demolished'
    public bool StackDemolished { get; private set; } = false;      // stack's player 'out'

    public bool StackFrozen { get; private set; } = false;

    private int restoreCount = 0;                               // number of times blocks in stack have been restored (pose)
    private bool stackRestored = false;

    private float EvenLevelRotation = 90f;              // even levels are rotated by 90 degrees

    private float LevelSpacingY = 0.1f;                 // vertical distance between levels in the stack (for level spawn position)
    public float CurrentLevelY { get; private set; } = 0;

    public float StackTopY => CurrentLevelY - LevelSpacingY;
    public float StackPlayerY = 0.025f;                   // player name positioned above stack name (StackTopY)

    private float blockDropInterval = 0.1f;            // delay between dropping blocks in stack

    // cardinal is used to determine the direction of the rows of runes in the rune slots
    public enum StackCardinal
    {
        East,
        South,
        West,
        North,
    };
    public StackCardinal Cardinal { get; private set; }

    private void OnEnable()
    {
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;        // sets stack to look at / follow
        GameEvents.OnCameraOrbitTable += OnCameraOrbitTable;        // look at / follow table

        GameEvents.OnRestoreBlocks += OnRestoreBlocks;
        GameEvents.OnEnablePhysics += OnEnablePhysics;
        GameEvents.OnDemolishStack += OnDemolishStack;
        GameEvents.OnFreezeStack += OnFreezeStack;
        GameEvents.OnChangeBlockStrength += OnChangeBlockStrength;
        GameEvents.OnResetStacks += OnResetStacks;
        GameEvents.OnReloadData += OnReloadData;
    }

    private void OnDisable()
    {
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable -= OnCameraOrbitTable;

        GameEvents.OnRestoreBlocks -= OnRestoreBlocks;
        GameEvents.OnEnablePhysics -= OnEnablePhysics;
        GameEvents.OnDemolishStack -= OnDemolishStack;
        GameEvents.OnFreezeStack -= OnFreezeStack;
        GameEvents.OnChangeBlockStrength -= OnChangeBlockStrength;
        GameEvents.OnResetStacks -= OnResetStacks;
        GameEvents.OnReloadData -= OnReloadData;
    }

    // sort blocks by strength in ascending order
    // build all the levels in this stack, grouping blocks into levels and then rows within each level
    // delay between each level for visual effect
    public void BuildLevels(string stackName, string playerName, List<BlockData> stackBlockData, StackData stackData)
    {
        stackTextFront.enabled = false;
        stackTextBack.enabled = false;
        stackTextTop.enabled = false;
        stackPlayerName.enabled = false;

        playerLight.enabled = false;

        // stackData is not yet ordered as required for block build order
        // sort blocks so strongest blocks are at the bottom of the stack
        orderedStackBlocks = stackBlockData?.OrderByDescending(x => x.initialStrength).ToList();        
        StackLevels = new();

        StackName = stackTextFront.text = stackTextBack.text = stackTextTop.text = stackName;
        PlayerName = stackPlayerName.text = playerName;
        CurrentLevelY = transform.position.y;

        List<BlockData> currentLevelBlocks = new();        // list of blocks for each level
        int maxBlocksPerLevel = stackData.rowsPerLevel * stackData.blocksPerRow;

        // iterate through all blocks in this stack, grouping into levels of (up to) (rowsPerLevel * blocksPerRow) blocks each
        foreach (var block in orderedStackBlocks)
        {
            currentLevelBlocks.Add(block);
     
            if (currentLevelBlocks.Count == maxBlocksPerLevel)     // new group of blocks in level
            {
                BuildLevels(currentLevelBlocks, stackData);
                currentLevelBlocks.Clear();                     // reset for next group of blocks
            }
        }

        // build a final (partial) level for any remaining blocks
        if (currentLevelBlocks.Count > 0)
        {
            BuildLevels(currentLevelBlocks, stackData);
        }

        // position stack name and player name at top of stack
        stackTextTop.transform.position = new Vector3(stackTextTop.transform.position.x, StackTopY, stackTextTop.transform.position.z);
        stackPlayerName.transform.position = new Vector3(stackTextTop.transform.position.x, StackTopY + StackPlayerY, stackTextTop.transform.position.z);
    }

    private void BuildLevels(List<BlockData> levelBlocks, StackData stackBuildData)
    {
        StackBuildData = stackBuildData;

        // new stack level, child of this stack
        var newLevel = Instantiate(stackLevelPrefab, transform);

        StackLevels.Add(newLevel);
        newLevel.name = $"Level {StackLevels.Count}";

        // offset level position
        newLevel.transform.position = new Vector3(transform.position.x, CurrentLevelY, transform.position.z);
        CurrentLevelY += LevelSpacingY;

        // split levelBlocks into BlockRows to populate newLevel
        newLevel.BuildRows(levelBlocks, this, stackBuildData);

        // even numbered levels are rotated 90 degrees (after child blocks built)
        bool evenLevel = StackLevels.Count % 2 == 0;
        if (evenLevel)
            newLevel.transform.rotation = Quaternion.Euler(0, EvenLevelRotation, 0);
    }

    // drop blocks onto table in the order they were built
    // complete each stack in turn
    public IEnumerator DropBlocks()
    {
        foreach (var level in StackLevels)
        {
            foreach (var row in level.LevelRows)
            {
                foreach (var block in row.RowBlocks)
                {
                    block.Drop(level.transform.position.y, true);
                    //yield return new WaitForSeconds(blockDropInterval);
                }
            }
            yield return new WaitForSeconds(blockDropInterval);     // drop a level at a time

            if (blockDropAudio != null)
                AudioSource.PlayClipAtPoint(blockDropAudio, level.transform.position);
        }

        // all blocks in place
        GameEvents.OnStackBuilt?.Invoke(this);

        yield return StartCoroutine(stackRunes.ShowSlots());        // show rune slots
    }

    // called by StackBlock on taking damage
    public void StackDamaged()
    {
        GetStackStrength(out float initialStrength, out float currentStrength);
        GameEvents.OnStackDamaged?.Invoke(this, initialStrength, currentStrength);
    }

    // total strength of all undemolished blocks in the stack
    private void GetStackStrength(out float initialStrength, out float currentStrength)
    {
        initialStrength = 0;
        currentStrength = 0;

        foreach (var level in StackLevels)
        {
            foreach (var row in level.LevelRows)
            {
                foreach (var block in row.RowBlocks)
                {
                    initialStrength += block.BlockData.initialStrength;
                    currentStrength += block.BlockData.currentStrength;
                }
            }
        }
    }

    private void OnRestoreBlocks(Stack stack, bool restoreStrength)
    {
        if (stack != this || StackDemolished)
            return;

        if (restoreStrength)
            GameEvents.OnStackDamaged?.Invoke(stack, 1f, 1f);       // stack back to full health!

        if (! stackRestored)                            // resets when current stack changed
        {
            stackRestored = true;

            restoreCount++;
            if (restoreCount > 1 && stackRestoreAudio != null)
                AudioSource.PlayClipAtPoint(stackRestoreAudio, transform.position);
        }
    }

    // called by a block in this stack when it is demolished or falls
    // set stack to demolished if fallen blocks have reached the demolition threshold
    public void CheckDemolishedThreshold(StackBlock block)
    {
        if (block.ParentStack != this)
            return;

        if (StackDemolished)
            return;

        if (StackFrozen)
            return;

        // calculate fallen block percentage (eg. for UI)
        float fallenPercent = Mathf.Clamp(FallenBlockPercent, 0f, 1f);
        GameEvents.OnStackFallenBlocks?.Invoke(this, fallenPercent);

        // whole stack is demolished when demolished block count exceeds threshold...
        if (NetworkInterface.Instance.IsLocalPlayersTurn && (FallenBlockCount + DemolishedBlockCount) >= StackDemolishedThreshold)
        {
            NetworkInterface.Instance.NetworkDemolishStack(StackName);      // utilimately fires OnDemolishStack event locally
        }
    }

    // event fired by ClientRPC for all players
    private void OnDemolishStack(string stackName)
    {
        if (StackName == stackName)
            Demolish();
    }

    private void Demolish()
    {
        StackDemolished = true;
        spotLight.color = playerLight.color = demolishedLightColour;

        GameEvents.OnFreezeStack?.Invoke(this, false);  // unfreeze!
        GameEvents.OnStackDemolished?.Invoke(this);     // ie. this stack's player is 'out'

        if (stackDemolishedAudio != null)
            AudioSource.PlayClipAtPoint(stackDemolishedAudio, transform.position);
    }

    // subtle sound effect as blocks in stack 'sag' under gravity
    private void OnEnablePhysics(bool enable, Stack stack)
    {
        if (stack != this)
            return;

        if (StackDemolished)
            return;

        if (enable && stackRestoreAudio != null)
            AudioSource.PlayClipAtPoint(stackRestoreAudio, transform.position);
    }


    private void OnFreezeStack(Stack stack, bool freeze)
    {
        if (stack != this)
            return;

        StackFrozen = freeze;
    }

    private void OnChangeBlockStrength(Stack stack, float totalStrength)
    {
        if (stack != this || totalStrength == 0)
            return;

        //int totalBlocks = BlockCount(out int fallenBlocks, out int demolishedBlocks);
        //int remainingBlocks = stackBlockCount - demolishedBlockCount;             //TODO: this?
        int remainingBlocks = StackBlockCount - FallenBlockCount;               // don't strength fallen blocks
        float strengthPerBlock = totalStrength / remainingBlocks;

        foreach (var level in StackLevels)
        {
            foreach (var row in level.LevelRows)
            {
                foreach (var block in row.RowBlocks)
                {
                    if (!block.BlockData.IsDemolished && !block.BlockData.IsFallen)
                        block.Strengthen(strengthPerBlock);
                }
            }
        }
    }

    private void OnResetStacks()
    {
        StopAllCoroutines();
    }

    private void OnReloadData()
    {
        StopAllCoroutines();
    }

    public void DestroyLevels()
    {
        StopAllCoroutines();

        foreach (var level in StackLevels)
        {
            level.DestroyBlocks();
            Destroy(level.gameObject);
        }

        StackLevels.Clear();
    }

    public void SetCardinal(StackCardinal cardinal)
    {
        Cardinal = cardinal;
    }

    // assign a player to this stack
    // on stack built
    public void AssignPlayer(StackPlayer player)
    {
        StackPlayer = player;

        stackTextTop.color = stackTextBack.color = stackTextBack.color = StackPlayer.PlayerColour;
        stackPlayerName.color = StackPlayer.PlayerColour;
        stackTextFront.enabled = true;
        stackTextBack.enabled = true;
        stackTextTop.enabled = true;
        stackPlayerName.enabled = true;

        playerLight.color = StackPlayer.PlayerColour;
        playerLight.enabled = true;
    }

    private void OnCameraOrbitStack(Stack stack)
    {
        spotLight.enabled = stack == this;

        if (stack == this)
            stackRestored = false;
    }

    private void OnCameraOrbitTable()
    {
        spotLight.enabled = false;
    }

    //// returns number of blocks in stack
    //public int BlockCount(out int fallen, out int demolished)
    //{
    //    int blockCount = 0;
    //    fallen = 0;
    //    demolished = 0;

    //    foreach (var level in StackLevels)
    //    {
    //        foreach (var row in level.LevelRows)
    //        {
    //            foreach (var block in row.RowBlocks)
    //            {
    //                blockCount++;

    //                if (block.BlockData.IsDemolished)
    //                    demolished++;

    //                if (block.BlockData.IsFallen)
    //                    fallen++;
    //            }
    //        }
    //    }
    //    return blockCount;
    //}
}
