using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// a linear row of blocks - a BlockLevel comprises a series of StackRows

public class BlockRow : MonoBehaviour
{
    [SerializeField] private StackBlock stoneBlockPrefab;
    [SerializeField] private StackBlock woodBlockPrefab;
    [SerializeField] private StackBlock glassBlockPrefab;

    [SerializeField] private AudioClip rowDropAudio;

    public List<StackBlock> RowBlocks { get; private set; } = new();
    public StackBlock GetBlockById(int id) => RowBlocks.FirstOrDefault(x => x.BlockData.blockId == id);

    //public float FallenBlockPercent => RowBlocks.Where(block => block.BlockData.IsFallen).Count() / RowBlocks.Count;
    //public int DemolishedBlocks => RowBlocks.Where(block => block.BlockData.IsDemolished).Count();

    [SerializeField] private Transform stackRunes;       // parent for all runes in this row - each block refs its own rune(s)
    public Transform StackRunes => stackRunes;

    private float currentBlockZ = 0;
    private float blockDropHeight = 1.75f;                 // on block instantiation - drop onto stack with LeanTween (not physics)

    private StackData stackBuildData;
    private Stack parentStack;
    private StackLevel parentLevel;
    public bool IsBaseLevel => parentLevel.IsBaseLevel;

    public void BuildBlocks(List<BlockData> rowBlocks, Stack parentStack, StackLevel parentLevel, StackData stackData)
    {
        stackBuildData = stackData;

        this.parentStack = parentStack;
        this.parentLevel = parentLevel;
        RowBlocks.Clear();

        int blockCounter = 0;
        float blockInterval = stackData.BlockLength + stackData.rowBlockSpacing;      // ie. block length + row gap

        bool evenBlocksPerRow = stackData.blocksPerRow % 2 == 0;

        // build out rows from centre - set first row position
        if (evenBlocksPerRow)
            currentBlockZ = transform.position.z + (blockInterval / 2f);    // even number of blocks - first block offset
        else
            currentBlockZ = transform.position.z;                         // odd number of blocks - first block centered

        foreach (BlockData blockData in rowBlocks)
        {
            StackBlock blockPrefab = woodBlockPrefab;     // TODO: set according to currentStrength?

            // new block, child of this row
            StackBlock newBlock = Instantiate(blockPrefab, transform);
            //newBlock.Init(blockData, parentStack, this);

            // set position of block within row
            IncrementBlockPosition(blockCounter, blockInterval);
            var blockPosition = new Vector3(transform.position.x, transform.position.y, currentBlockZ);     // block position in row, once dropped

            // block raised up to drop height
            newBlock.transform.position = new Vector3(blockPosition.x, blockPosition.y + blockDropHeight, blockPosition.z);
            newBlock.Init(blockData, parentStack, this); //, blockPosition);

            RowBlocks.Add(newBlock);
            newBlock.name = $"Block {RowBlocks.Count}";

            blockCounter++;
        }
    }

    private void IncrementBlockPosition(int blockCounter, float blockInterval)
    {
        bool evenBlockCounter = (blockCounter % 2 == 0);

        if (evenBlockCounter)
            currentBlockZ += (blockCounter * blockInterval);
        else
            currentBlockZ -= (blockCounter * blockInterval);
    }

    public void DestroyBlocks()
    {
        StopAllCoroutines();

        foreach (var block in RowBlocks)
        {
            block.DestroyBlock();       // and rune
        }

        RowBlocks.Clear();
    }
}

