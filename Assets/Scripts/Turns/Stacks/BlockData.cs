
using UnityEngine;

// data for each Jenga block

[System.Serializable]
public class BlockData
{
    public int blockId;                         // unique id (across all stacks)
    public string stackName;                    // Stack1, 2, 3, 4
    public string playerName;                   // a player can control >1 stack
    public float initialStrength = 100f;
    public float currentStrength = 100f;

    public int HealthPercent => initialStrength > 0 ? ((int)(currentStrength / initialStrength * 100f)) : 0;

    public Vector3 blockSize;

    public bool IsDemolished = false;
    public bool IsFallen = false;         // hit table or floor
    public bool IsSelected;               // raycast hit, for damage
    public bool IsFrozen = false;         // via rune
    public bool PoseRestored = false;     // via raycast
}
