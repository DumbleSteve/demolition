using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


// builds stacks of blocks from data fetched from a web request
// fetched data has been deserialised into a List<BlockData>, but is not yet sorted
// as required for the order in which blocks in the stack are to be built

public class StackBuilder : MonoBehaviour
{
    // scriptable object to define stack build parameters,
    // if not built from web request json data
    [SerializeField] private StackData stackBuildData;                      // set in inspector
    private List<BlockData> allBlockBuildList = new();                      // populated by ConstructStackData

    [Space]
    [SerializeField] private Transform stackCentrePosition;                 // centre of square of stacks
    [SerializeField] private Stack stackPrefab;

    private List<Stack> allStacks;                                          // all stacks built by this builder

    private List<BlockData> orderedBlockData;                               // ordered by stack name, for grouping into stacks
    private Dictionary<string, List<BlockData>> stackBlockDict = new();     // key is stack name

    // stacks built in a square
    private bool stackRunesFacingOut = false;
    private float stackSpacingFacingOut = 1.3f;                             // horizontal distance between stacks on the table
    private float stackSpacingFacingIn = 1.6f;                              // horizontal distance between stacks on the table
    //private float currentStackX = 0;                                      // x position while building in a row

    private int stackBuildCounter = 0;                                      
    private float stackRotationY = 0f;                                  // for rune slots position

    private void OnEnable()
    {
        GameEvents.OnConstructStackBuildData += OnConstructStackBuildData;  // using player names from lobby when all netcode players spawned
        GameEvents.OnReadyToBuildStacks += OnReadyToBuildStacks;            // via lobby game start -> TurnManager records spawned NetcodePlayers -> build stacks!

        GameEvents.OnBuildStacks += OnBuildStacks;
        GameEvents.OnResetStacks += OnResetStacks;
        GameEvents.OnDestroyStacks += OnDestroyStacks;
        //GameEvents.OnReloadData += OnReloadData;
    }

    private void OnDisable()
    {
        GameEvents.OnConstructStackBuildData -= OnConstructStackBuildData;
        GameEvents.OnReadyToBuildStacks -= OnReadyToBuildStacks;

        GameEvents.OnBuildStacks -= OnBuildStacks;
        GameEvents.OnResetStacks -= OnResetStacks;
        GameEvents.OnDestroyStacks -= OnDestroyStacks;
        //GameEvents.OnReloadData -= OnReloadData;
    }

    private void Start()
    {
        LeanTween.init(3200);
    }

    // all netcode players spawned and player names from lobby assigned
    private void OnConstructStackBuildData(List<LobbyPlayerData> lobbyPlayerNames)
    {
        // construct BlockData list from a new StackData
        // (as opposed to fetching from web request)
        // using player names/sprites from lobby
        ConstructStackData(lobbyPlayerNames);
    }

    // all netcode players spawned and stack build data constructed using player names/sprites from lobby
    private void OnReadyToBuildStacks()
    {
        GameEvents.OnBuildStacks?.Invoke(allBlockBuildList, stackBuildData);
    }

    // build a BlockData list from stackBuildData SO to 'stream' into stack builder
    // so that such a list can be retrieved from a web request or database in future
    // to allow saving of each block's state
    private void ConstructStackData(List<LobbyPlayerData> lobbyPlayerNames)
    {
        allBlockBuildList = new();
        int blockCounter = 0;           // becomes unique block id

        for (int s = 0, playerIndex = 0; s < stackBuildData.stacksToBuild; s++, playerIndex++)
        {
            if (playerIndex >= lobbyPlayerNames.Count)      // cycle through players if fewer than stacks
                playerIndex = 0;

            string playerName = lobbyPlayerNames[playerIndex].PlayerName;
            string stackName = $"{stackBuildData.stackName} {s+1}";

            for (int l = 0; l < stackBuildData.levelsPerStack; l++)
            {
                // levels build up from bottom of stack, strongest at bottom (first)
                float levelStrength = stackBuildData.maxBlockStrength - (stackBuildData.LevelStrengthStep * l);

                for (int r = 0; r < stackBuildData.rowsPerLevel; r++)
                {
                    for (int b = 0; b < stackBuildData.blocksPerRow; b++)
                    {
                        blockCounter++;

                        var blockData = new BlockData
                        {
                            blockId = blockCounter,
                            playerName = playerName,
                            stackName = stackName,
                            initialStrength = levelStrength,
                            currentStrength = levelStrength,

                            blockSize = stackBuildData.blockSize,

                            //runeName = StackRunes.RandomRune 
                        };

                        //if (blockData.runeName == "")
                        //    Debug.Log($"BuildStackData: NO RUNE for block {l}/{r}/{b} in stack {blockData.stackName}");

                        allBlockBuildList.Add(blockData);
                    }
                }
            }
        }
    }

    //private StackBlock GetBlockById(int id)
    //{
    //    foreach (var stack in allStacks)
    //    {
    //        foreach (var level in stack.StackLevels)
    //        {
    //            foreach (var row in level.LevelRows)
    //            {
    //                foreach (var block in row.RowBlocks)
    //                {
    //                    if (block.BlockData.blockId == id)
    //                        return block;
    //                }
    //            }
    //        }
    //    }
    //    return null;
    //}


    // web request has returned json results, which have been deserialized into a list
    // or BlockData list contructed from a new StackData instance
    // sort by stack name in order to construct a dictionary, keyed by stack name
    private void OnBuildStacks(List<BlockData> allBlocks, StackData stackBuildData)
    {
        if (allBlocks == null || allBlocks.Count == 0)       // no data fetched!
            return;

        // orderedBlockData is ordered by name, as each stack name becomes its own stack
        orderedBlockData = allBlocks?.OrderBy(x => x.stackName).ToList();   // only one linq call for efficiency

        stackBlockDict = new();         // dictionary, keyed by stack name
        allStacks = new();

        // organise into dictionary, keyed by stack name, value is unsorted BlockData list (will be sorted by each stack)
        BuildStackDictionary();         // only one linq call for efficiency

        // build a stack for each stack name, using each key in the dictionary
        BuildStacks(stackBuildData.stacksToBuild, stackBuildData);
    }

    // build a Stack for each name in the dictionary
    // build each Stack in turn, then each StackLevel in turn, then rows, then blocks
    private void BuildStacks(int stacksToBuild, StackData stackBuildData)
    {
        stackBuildCounter = 0;

        // stacks rotated to face each side of the table
        stackRotationY = stackRunesFacingOut ? -90f : 90f;      // first stack

        foreach (var stackName in stackBlockDict.Keys)
        {
            stackBuildCounter++;

            // a bit clumsy...
            // all BlockData objects in this stack's blocks have the same playerName,
            // so use playerName from first block to build the stack
            List<BlockData> stackData = stackBlockDict[stackName];
            var playerName = stackData[0].playerName;

            BuildStack(stackName, playerName, stackBuildData);

            // don't exeed stack limit (won't fit on table!)
            if (allStacks.Count == stacksToBuild)
                break;
        }

        StartCoroutine(DropStacks());   // drop each block in each stack in turn
    }

    private void BuildStack(string stackName, string playerName, StackData stackData)
    {
        // new stack, child of this builder
        Stack newStack = Instantiate(stackPrefab, transform);
        newStack.name = stackName;

        // stacks built in a square...
        Vector3 stackCentre = stackCentrePosition.position;
        var stackSpacing = stackRunesFacingOut ? stackSpacingFacingOut : stackSpacingFacingIn;

        // each player 'sits' at each side of the table
        switch (stackBuildCounter)
        {
            case 1:
                newStack.transform.position = new Vector3(stackCentre.x + stackSpacing, stackCentre.y, stackCentre.z);
                newStack.SetCardinal(Stack.StackCardinal.East);
                break;

            case 2:
                newStack.transform.position = new Vector3(stackCentre.x, stackCentre.y, stackCentre.z - stackSpacing);
                newStack.SetCardinal(Stack.StackCardinal.South);
                break;

            case 3:
                newStack.transform.position = new Vector3(stackCentre.x - stackSpacing, stackCentre.y, stackCentre.z);
                newStack.SetCardinal(Stack.StackCardinal.West);
                break;

            case 4:
                newStack.transform.position = new Vector3(stackCentre.x, stackCentre.y, stackCentre.z + stackSpacing);
                newStack.SetCardinal(Stack.StackCardinal.North);
                break;

            default:        // any extra stacks built in centre (shouldn't happen!)
                newStack.transform.position = stackCentre;
                break;
        }

        // stacks built in a row...
        //newStack.transform.position = new Vector3(currentStackX, firstStackPosition.position.y, firstStackPosition.position.z);
        //newStack.transform.position = stackBuildPosition;
        //currentStackX += stackSpacing;

        allStacks.Add(newStack);

        // build all levels for stackName
        // get list of blocks for stackName from dictionary
        List<BlockData> stackBlockData = stackBlockDict [ stackName ];

        newStack.BuildLevels(stackName, playerName, stackBlockData, stackData);

        newStack.transform.rotation = Quaternion.Euler(new Vector3(0f, stackRotationY, 0f));    // stack rotated to face side of the table
        stackRotationY += 90f;          // for next stack
    }

    // orderedBlockData is ordered by stack name only
    private void BuildStackDictionary()
    {
        string stackName = "";         // new stack for each stack name

        // organise into dictionary, keyed by stack name, value is unsorted BlockData list
        foreach (var block in orderedBlockData)
        {
            if (block.stackName != stackName)
            {
                stackName = block.stackName;

                var stackData = orderedBlockData.Where(x => x.stackName == stackName).ToList();

                stackBlockDict.Add(stackName, stackData);
            }
        }
    }

    // drop each block in each stack in turn
    private IEnumerator DropStacks()
    {
        foreach (var stack in allStacks)
        {
            // camera looks at new stack as it's being 'built'...
            GameEvents.OnCameraOrbitStack?.Invoke(stack);

            yield return StartCoroutine(stack.DropBlocks());
        }

        GameEvents.OnAllStacksBuilt?.Invoke(allStacks);
    }

    private void OnResetStacks()
    {
        // destroy all stacks (and therefore levels and blocks)
        GameEvents.OnDestroyStacks?.Invoke();

        // rebuild a stack for each stack name, from the dictionary
        BuildStacks(stackBuildData.stacksToBuild, stackBuildData);
    }

    private void OnDestroyStacks()
    {
        StopAllCoroutines();

        // destroy all stacks (and therefore levels and blocks)
        foreach (var stack in allStacks)
        {
            stack.DestroyLevels();

            Destroy(stack.gameObject);
        }

        allStacks.Clear();
    }

    //private void OnReloadData()
    //{
    //    // destroy all stacks (and therefore levels and blocks)
    //    GameEvents.OnDestroyStacks?.Invoke();

    //    // initiate web request to fetch stack data
    //    GameEvents.OnFetchStackData?.Invoke();          // this is listening for OnStackDataFetched callback event
    //}

}
