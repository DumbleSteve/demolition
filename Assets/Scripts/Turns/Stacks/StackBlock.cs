using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(AudioSource))]

// a single block in a StackLevel within a Stack
// encapsulates a BlockData struct, set according to data from web request
// handles its own demolition and plays audio/particle effects

public class StackBlock : MonoBehaviour
{
    [SerializeField] private RuneLibrary runeLibrary;           // scriptable object

    [SerializeField] private ParticleSystem damageParticles;
    [SerializeField] private ParticleSystem demolishParticles;

    [SerializeField] private AudioClip damageAudio;
    [SerializeField] private AudioClip explosionAudio;
    [SerializeField] private AudioClip smashAudio;

    [SerializeField] private AudioClip collisionAudio;
    private AudioSource collisionSource;                        // child component - to allow pitch adjust for different materials

    [SerializeField] private AudioClip blockDropAudio;

    [SerializeField] private TextMeshPro label1;                // mastery, front
    [SerializeField] private TextMeshPro label2;                // mastery, back

    private Material blockMaterial;                             // at start
    [SerializeField] private Material hilightMaterial;
    [SerializeField] private Material frozenMaterial;
    [SerializeField] private Material fallenMaterial;

    [SerializeField] private Color blockTextColour = Color.black;
    [SerializeField] private Color blockDamageColour = Color.red;
    [SerializeField] private Color blockStrengthenColour = Color.green;
    [SerializeField] private Color stackDemolishedColour = Color.magenta;

    private float textDamageTime = 0.5f;
    private float textDamageColourPause = 0.75f;

    public BlockData BlockData; // show in inspector  { get; private set; }

    private float blockDropTime = 0.1f;
    private float restoreTime = 0.2f;

    private float recoilScale = 1.15f;
    private float recoilTime = 0.15f;
    private bool recoiling = false;

    public Stack ParentStack { get; private set; }
    private BlockRow parentRow;

    public Rune BlockRune { get; private set; } = null;           // lookup and instantiate prefab from library using BlockData.RuneName

    private Pose startPose;
    private Vector3 startScale;                 // set by Init from BlockData

    private Rigidbody blockRigidbody;
    private Collider blockCollider;
    private Renderer blockRenderer;
    private Outline blockOutline;


    private void Awake()
    {
        blockRigidbody = GetComponent<Rigidbody>();
        blockCollider = GetComponent<Collider>();
        blockRenderer = GetComponent<Renderer>();
        blockRenderer = GetComponent<Renderer>();

        blockMaterial = blockRenderer.material;

        blockOutline = GetComponent<Outline>();

        collisionSource = GetComponent<AudioSource>();
        collisionSource.clip = collisionAudio;

        label1.color = label2.color = blockTextColour;
    }

    private void OnEnable()
    {
        GameEvents.OnEnablePhysics += OnEnablePhysics;
        GameEvents.OnRestoreBlocks += OnRestoreBlocks;          // return to start position and rotation
        GameEvents.OnFreezeStack += OnFreezeStack;  
        GameEvents.OnDetonate += OnDetonate;                    // do damage to selected blocks
        GameEvents.OnStackDemolished += OnStackDemolished;
        GameEvents.OnRaycastHit += OnRaycastHit;                // hilight multiple blocks
        GameEvents.OnDemolishBlock += OnDemolishBlock;          // networked, so same rune for all players
    }

    private void OnDisable()
    {
        GameEvents.OnEnablePhysics -= OnEnablePhysics;
        GameEvents.OnRestoreBlocks -= OnRestoreBlocks;
        GameEvents.OnFreezeStack -= OnFreezeStack;
        GameEvents.OnDetonate -= OnDetonate;
        GameEvents.OnStackDemolished -= OnStackDemolished;
        GameEvents.OnRaycastHit -= OnRaycastHit;
        GameEvents.OnDemolishBlock -= OnDemolishBlock;
    }

    private void OnRaycastHit(List<StackBlock> blocks, Rune hitRune)
    {
        if (ParentStack.StackDemolished)
            return;

        if (blocks != null)
            Hilight(blocks.Contains(this));
        else
            Hilight(false);
    }

    private void Hilight(bool hilight)
    {
        if (hilight && ParentStack.StackDemolished)
            return;

        BlockData.IsSelected = hilight;
        blockOutline.enabled = hilight;
        blockRenderer.material = hilight ? hilightMaterial : (BlockData.IsFrozen ? frozenMaterial :
                                                (BlockData.IsFallen ? fallenMaterial : blockMaterial));
    }

    private void OnDetonate(float damagePerBlock, Rune hitRune)
    {
        if (ParentStack.StackDemolished)
            return;

        if (ParentStack.SelectedBlockCount == 0)
            return;

        // disable physics and restore block's pose
        OnEnablePhysics(false, ParentStack);
        OnRestoreBlocks(ParentStack, false);

        if (BlockData.IsSelected)
            TakeDamage(damagePerBlock);
    }

    private void TakeDamage(float damage)
    {
        BlockData.currentStrength -= damage;

        if (BlockData.currentStrength <= 0)
        {
            BlockData.currentStrength = 0;
            Demolish();
        }
        else
        {
            Recoil();

            if (damageParticles != null)
                damageParticles.Play();

            if (damageAudio != null)
                AudioSource.PlayClipAtPoint(damageAudio, transform.position);

            //Debug.Log($"TakeDamage {name}: before {label1.text}  after {BlockLabel()}");
            label1.text = label2.text = BlockLabel();

            BlockData.IsSelected = false;
            Hilight(false);

            // block text turns red to show damage taken
            label1.color = label2.color = blockDamageColour;
            LeanTween.value(gameObject, blockDamageColour, blockTextColour, textDamageTime)
                            .setDelay(textDamageColourPause)
                            .setOnUpdate((Color c) => label1.color = label2.color = c);
        }

        // stack fires event every time it is damaged (eg. for health in UI)
        ParentStack.StackDamaged();
    }

    // increases/decreases block initial strength
    // and current strength by same percentage
    public void Strengthen(float blockStrengthIncrease)
    {
        if (ParentStack.StackDemolished)
            return;

        if (blockStrengthIncrease == 0)
            return;

        // current strength percentage
        float currentStrengthPercent = BlockData.currentStrength / BlockData.initialStrength;

        //Debug.Log($"Strengthen {name}: blockStrengthIncrease = {blockStrengthIncrease}  currentStrengthPercent = {currentStrengthPercent}");

        BlockData.initialStrength += blockStrengthIncrease;

        if (BlockData.initialStrength <= 0)
        {
            BlockData.initialStrength = 0;
            BlockData.currentStrength = 0;
            Demolish();
            return;
        }

        // change current strength by same percentage
        BlockData.currentStrength = BlockData.initialStrength * currentStrengthPercent;

        if (BlockData.currentStrength <= 0)
        {
            BlockData.currentStrength = 0;
            Demolish();
            return;
        }

        //Debug.Log($"Strengthen {name}: before {label1.text}  after {BlockLabel()}");
        label1.text = label2.text = BlockLabel();

        // block text turns red or green to show strength increase or decrease
        var feedbackColour = blockStrengthIncrease < 0 ? blockDamageColour : blockStrengthenColour;
        label1.color = label2.color = feedbackColour;
        LeanTween.value(gameObject, feedbackColour, blockTextColour, textDamageTime)
                        .setDelay(textDamageColourPause)
                        .setOnUpdate((Color c) => label1.color = label2.color = c);

        // stack fires event every time it is damaged/strengthened (eg. for health in UI)
        ParentStack.StackDamaged();
    }

    private void Demolish()
    {
        if (ParentStack.StackDemolished)
            return;

        if (ParentStack.StackFrozen )
            return;

        if (BlockData.IsDemolished)  
            return;

        BlockData.IsDemolished = true;
        blockCollider.enabled = false;
        blockRenderer.enabled = false;

        label1.enabled = false;
        label2.enabled = false;

        BlockData.IsSelected = false;
        Hilight(false);

        //if (ParentStack.StackBuildData.RandomRuneChance)
            NetworkInterface.Instance.NetworkDemolishBlock(BlockData.blockId, ParentStack.StackBuildData.RandomRuneChance);      // ultimately fires OnDemolishBlock event locally (see below)
    }

    // NetworkDemolishBlock 'callback'
    private void OnDemolishBlock(int blockId, string runeName)
    {
        if (BlockData.blockId != blockId)       // not this block being demolished!
            return;

        if (! string.IsNullOrEmpty(runeName))
        {
            // spawn rune!
            var runeData = runeLibrary.RuneByName(runeName);
            if (runeData != null && runeData.RunePrefab != null)
            {
                BlockRune = Instantiate(runeData.RunePrefab, parentRow.StackRunes);     // parent to row to avoid scaling issues and rotate with level
                BlockRune.Init(runeData, this, transform.position);

                GameEvents.OnRuneSpawned?.Invoke(BlockRune);            // cached for lookup by block id
                BlockRune.Reveal();
            }
        }

        if (demolishParticles != null)
            demolishParticles.Play();

        if (explosionAudio != null)
            AudioSource.PlayClipAtPoint(explosionAudio, transform.position);

        if (smashAudio != null)
            AudioSource.PlayClipAtPoint(smashAudio, transform.position);

        ParentStack.CheckDemolishedThreshold(this);
    }

    private void OnStackDemolished(Stack stack)
    {
        if (stack != ParentStack)
            return;

        label1.color = label2.color = stackDemolishedColour;
        blockRenderer.material = fallenMaterial;
    }

    private void OnEnablePhysics(bool enable, Stack stack)
    {
        if (stack != null && stack != ParentStack)
            return;

        if (BlockData.IsDemolished)
            return;

        blockRigidbody.isKinematic = !enable;

        if (enable)
            BlockData.PoseRestored = false;
    }

    private void OnRestoreBlocks(Stack stack, bool resetStack)
    {
        if (stack != null && stack != ParentStack) 
            return;

        if (ParentStack.StackDemolished)
            return;

        if (resetStack)
            Reset();          // includes demolished blocks

        SetFallen(false);
        RestorePose();
    }

    private void RestorePose()
    {
        if (BlockData.PoseRestored || (transform.position == startPose.position && transform.rotation == startPose.rotation))
            return;

        LeanTween.move(gameObject, startPose.position, restoreTime)
                .setEaseOutCirc();

        LeanTween.rotate(gameObject, startPose.rotation.eulerAngles, restoreTime)
                .setEaseOutCirc()
                .setOnComplete(() => BlockData.PoseRestored = true);
    }

    private void OnFreezeStack(Stack stack, bool freeze)
    {
        if (stack != null && stack != ParentStack)
            return;

        if (freeze && ParentStack.StackDemolished)
            return;

        BlockData.IsFrozen = freeze;

        if (frozenMaterial != null)
            blockRenderer.material = BlockData.IsFrozen ? frozenMaterial : (BlockData.IsFallen ? fallenMaterial : blockMaterial);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // set to fallen if hits table, floor or revealed rune
        bool isFallen = collision.gameObject.CompareTag("Table") || collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Rune");
        if (isFallen)
        {
            if (collisionAudio != null)
                collisionSource.Play();

            if (ParentStack.StackFrozen)
                return;

            // blocks in the base level can't fall... even though technically they collide with table when physics enabled
            if (!parentRow.IsBaseLevel && !BlockData.IsFallen)
            {
                SetFallen(true);
                ParentStack.CheckDemolishedThreshold(this);
            }
        }
    }

    private void Recoil()
    {
        if (recoiling)
            return;

        LeanTween.scale(gameObject, startScale * recoilScale, recoilTime)
                    .setEaseOutElastic()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(gameObject, startScale, recoilTime)
                            .setEaseOutBack()
                            .setOnComplete(() =>
                            {
                                recoiling = false;
                            });
                    });
    }

    // associate block with BlockData (eg. fetched from web request)
    // spawn rune if block has a runeName
    public void Init(BlockData block, Stack parentStack, BlockRow parentRow) //, Vector3 runePosition)
    {
        BlockData = block;
        ParentStack = parentStack;
        this.parentRow = parentRow;

        startScale = transform.localScale = new Vector3(BlockData.blockSize.x, BlockData.blockSize.y, BlockData.blockSize.z);

        label1.text = label2.text = BlockLabel();
    }

    private void Reset()
    {
        BlockData.IsDemolished = false;
        BlockData.IsFallen = false;
        BlockData.currentStrength = BlockData.initialStrength;

        blockRenderer.enabled = true;
        blockRenderer.material = blockMaterial;
        blockCollider.enabled = true;
        blockRigidbody.isKinematic = true;

        label1.text = label2.text = BlockLabel();
        label1.enabled = label2.enabled = true;

        // reset state
        recoiling = false;
        BlockData.IsSelected = false;
        BlockData.IsFrozen = false;
    }

    // drop with LeanTween (not physics, as activating physics causes stack to wobble etc)
    public void Drop(float toPositionY, bool silent)
    {
        LeanTween.moveY(gameObject, toPositionY, blockDropTime)
                .setEaseInQuint()
                .setOnComplete(() =>
                {
                    if (!silent && blockDropAudio != null)
                        AudioSource.PlayClipAtPoint(blockDropAudio, transform.position);

                    startPose.position = transform.position;
                    startPose.rotation = transform.rotation;
                });
    }

    private string BlockLabel()
    {
        if (BlockData == null)
            return "???";

        //return string.Format("{0:0}%", BlockData.StrengthPercent);
        return $"{(int)BlockData.currentStrength}/{(int)BlockData.initialStrength} : {BlockData.HealthPercent}%";
    }

    public bool IsChildOfStack(Stack currentStack)
    {
        return ParentStack == currentStack;
    }

    public void DestroyBlock()
    {
        StopAllCoroutines();

        if (BlockRune != null)
            Destroy(BlockRune.gameObject);

        Destroy(gameObject);
    }

    private void SetFallen(bool fallen)
    {
        if (BlockData.IsFallen == fallen)
            return;

        BlockData.IsFallen = fallen;

        if (fallenMaterial != null)
            blockRenderer.material = BlockData.IsFrozen ? frozenMaterial : (fallen ? fallenMaterial : blockMaterial);
    }
}
