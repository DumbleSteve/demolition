using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// a level in a stack, contains max 3 StackBlocks arranged within a square

public class StackLevel : MonoBehaviour
{
    [SerializeField] private BlockRow blockRowPrefab;

    [SerializeField] private AudioClip levelDropAudio;

    public List<BlockRow> LevelRows { get; private set; } = new();
    //public float FallenBlockPercent => LevelRows.Average(row => row.FallenBlockPercent);
    //public int DemolishedBlocks => LevelRows.Sum(row => row.DemolishedBlocks);

    private float currentRowX = 0;

    private StackData stackData;
    private Stack parentStack;
    public bool IsBaseLevel => parentStack.StackLevels.IndexOf(this) == 0;


    // split levelBlocks (all blocks in level) into BlockRows to populate this level
    public void BuildRows(List<BlockData> levelBlocks, Stack parentStack, StackData stackData)
    {
        //Debug.Log($"{name} StackLevel.BuildRows: levelBlocks {levelBlocks.Count} blocksPerRow {maxBlocksPerRow}");

        this.stackData = stackData;
        this.parentStack = parentStack;
        LevelRows.Clear();

        int rowCounter = 0;
        float rowInterval = stackData.BlockWidth + stackData.levelRowSpacing;      // ie. block width + row gap

        bool evenRowsPerLevel = stackData.rowsPerLevel % 2 == 0;

        // build out rows from centre - set first row position
        if (evenRowsPerLevel)
            currentRowX = transform.position.x + (rowInterval / 2f);    // even number of rows - first row offset
        else
            currentRowX = transform.position.x;                         // odd number of rows - first row centered

        List <BlockData> currentRowBlocks = new();                      // list of blocks for each row

        // iterate through all blocks in this level, grouping into levels of (up to) maxBlocksPerRow blocks each
        foreach (var block in levelBlocks)
        {
            currentRowBlocks.Add(block);

            if (currentRowBlocks.Count == stackData.blocksPerRow)       // new group of blocks in row
            {
                IncrementRowPosition(rowCounter, rowInterval);

                BuildRow(currentRowBlocks, currentRowX);

                currentRowBlocks.Clear();                     // reset for next group of blocks
                rowCounter++;
            }
        }

        // build a final (partial) row for any remaining blocks
        if (currentRowBlocks.Count > 0)
        {
            IncrementRowPosition(rowCounter, rowInterval);
            BuildRow(currentRowBlocks, currentRowX);
        }
    }

    private void IncrementRowPosition(int rowCounter, float rowInterval)
    {
        bool evenRowCounter = (rowCounter % 2 == 0);

        if (evenRowCounter)
            currentRowX += (rowCounter * rowInterval);
        else
            currentRowX -= (rowCounter * rowInterval);
    }

    private void BuildRow(List<BlockData> rowBlocks, float positionX)
    {
        //Debug.Log($"    {name} StackLevel.BuildRow: rowBlocks {rowBlocks.Count}");

        // new row, child of this level
        BlockRow newRow = Instantiate(blockRowPrefab, transform);

        // set position of row in level
        newRow.transform.position = new Vector3(positionX, transform.position.y, transform.position.z);

        LevelRows.Add(newRow);
        newRow.name = $"Row {LevelRows.Count}";

        // build the blocks in newRow
        newRow.BuildBlocks(rowBlocks, parentStack, this, stackData);
    }

    public void DestroyBlocks()
    {
        foreach (var row in LevelRows)
        {
            row.DestroyBlocks();
            Destroy(row.gameObject);
        }

        LevelRows.Clear();
    }
}

