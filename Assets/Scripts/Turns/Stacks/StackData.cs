
using UnityEngine;

// data for building Jenga stacks

[CreateAssetMenu(menuName = "Stack Build Data", fileName = "Stack Build Data")]

[System.Serializable]
public class StackData : ScriptableObject
{
    public int stacksToBuild = 3;           // identical stacks

    public string stackName = "Stack";      // different names for different styles?

    public int levelsPerStack = 6;          // number of levels in stack

    public int rowsPerLevel = 3;            // max number of rows in each row
    public float levelRowSpacing = 0.065f;  // gap between each row in a level

    public int blocksPerRow = 1;            // max number of blocks in each row
    public float rowBlockSpacing = 0f;      // gap between each block in a row

    public float minBlockStrength = 100f;      // initial block strength for top level of stack
    public float maxBlockStrength = 300f;      // initial block strength for bottom level of stack

    public Vector3 blockSize = new(0.2f, 0.1f, 0.7f);    // x = width, y = height, z = length

    public float BlockWidth => blockSize.x;
    public float BlockLength => blockSize.z;
    public float BlockHeight => blockSize.y;

    public int BlocksPerLevel => rowsPerLevel * blocksPerRow;
    public float LevelStrengthStep => (maxBlockStrength - minBlockStrength) / (levelsPerStack - 1);

    public float RowLength => (blockSize.z * rowsPerLevel) + (rowBlockSpacing * (rowsPerLevel - 1));

    public float RuneChance = 0.5f;         // chance of rune vs no rune in each block (on block detonation)
    public bool RandomRuneChance => Random.Range(0f, 1f) <= RuneChance;

    public override string ToString()
    {
        return $"StackData: stacksToBuild={stacksToBuild} levelsPerStack={levelsPerStack} rowsPerLevel={rowsPerLevel} blocksPerRow={blocksPerRow}";
    }
}
