using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;
using Unity.Services.Lobbies.Models;


// handles turn damage countdown while blocks / runes are being selected
// for demolition via DamageUI button.
//
// manages players' turns in conjunction with NetworkTurnManager singleton

public class TurnManager : MonoBehaviour
{
    public static TurnManager Instance { get; private set; }       // singleton

    [SerializeField] private StackPlayer playerPrefab;
    public List<StackPlayer> AllStackPlayers { get; private set; } = new();
    public List<StackPlayer> OrderedStackPlayers => AllStackPlayers.OrderBy(p => p.Data.LobbyPlayerData.Joined).ToList();
    private StackPlayer currentTurnPlayer = null;
    private int CurrentPlayerTurnIndex => (currentTurnPlayer != null && AllStackPlayers != null) ? allStacks.IndexOf(currentTurnPlayer.PlayerStack) : -1;

    private int PlayersNoCharge => AllStackPlayers.Count(p => p.Data.MaxCharge <= 0);
    private int DemolishedStackCount => AllStackPlayers.Count(p => p.PlayerStack.StackDemolished);
    private int StacksOutCount => PlayersNoCharge + DemolishedStackCount;
    private bool AllStacksOut => StacksOutCount >= AllStackPlayers.Count;
    public bool LastStackStanding => StacksOutCount == AllStackPlayers.Count - 1;

    [SerializeField] private List<Color> playerColours = new();

    [SerializeField] private AudioClip stackChangedAudio;
    [SerializeField] private AudioClip gameOverAudio;               // all players 'out'

    [SerializeField] private AudioClip detonatorPlungerAudio;       // start push down
    [SerializeField] private AudioClip detonatorDownAudio;           // pushed down fully

    // 'charge' counts down every tickIntervalSeconds, resets at start of turn
    // safety net: use first player if currentTurnPlayer not yet set (it's a timing / network thing)
    private int CurrentPlayerMaxCharge => (currentTurnPlayer != null) ? currentTurnPlayer.Data.MaxCharge : OrderedStackPlayers[0].Data.MaxCharge;

    private List<Stack> allStacks;
    private Stack currentStack = null;
    //private int CurrentStackIndex => (currentStack != null && allStacks != null) ? allStacks.IndexOf(currentStack) : -1;

    private float tickInterval = 0.25f;                  // time between damageChargeRemaining decrements
    private float turnEndedTickInterval = 1f;           // countdown after detonation(s)
    private float tickTimeRemaining;                    // time until next tick to decrement charge

    private int resetAfterDetonation = 10;              // reset damageChargeRemaining (ie. ticks) after stack detonation -> next turn on timout (ie. if 'end turn' not clicked)

    private int damageChargeRemaining = 0;
    private bool turnCountdownRunning = false;
    private bool turnEnded = false;                     // if true, continue countdown, but don't update UI
    private int detonationCount = 0;                    // times detonate button clicked (for extra hits) - reset on start turn

    private readonly float timeOutTurnPause = 0.5f;     // short pause before turn start after turn timeout
    private readonly float enablePhysicsDelay = 0.5f;   // after detonation, for visual effect only


    private void Awake()
    {
        Instance = this;        // singleton
    }

    private void OnEnable()
    {
        GameEvents.OnStackBuilt += OnStackBuilt;
        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;
        GameEvents.OnNextPlayerTurn += OnNextPlayerTurn;            // end turn clicked or charge timed out
        GameEvents.OnTurnChanged += OnTurnChanged;                  // start of turn after current player set
        GameEvents.OnTurnEnd += OnTurnEnd;                          // on detonation or charge time out
        GameEvents.OnDetonate += OnDetonate;                        // detonator button clicked
        GameEvents.OnManualStackSelect += OnManualStackSelect;      // via SelectStackButton - doesn't change turn
        GameEvents.OnStackDemolished += OnStackDemolished;          // block detonated threshold reached
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;
        GameEvents.OnDetonatorActivated += OnDetonatorActivated;    // audio
        GameEvents.OnDetonatorDown += OnDetonatorDown;              // audio
    }

    private void OnDisable()
    {
        GameEvents.OnStackBuilt -= OnStackBuilt;
        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnNextPlayerTurn -= OnNextPlayerTurn;
        GameEvents.OnTurnChanged -= OnTurnChanged;
        GameEvents.OnTurnEnd -= OnTurnEnd;
        GameEvents.OnDetonate -= OnDetonate;               
        GameEvents.OnManualStackSelect -= OnManualStackSelect;
        GameEvents.OnStackDemolished -= OnStackDemolished;
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnDetonatorActivated -= OnDetonatorActivated;
        GameEvents.OnDetonatorDown -= OnDetonatorDown;
    }

    private void Start()
    {
        GameEvents.OnTurnChargeTick?.Invoke(CurrentPlayerMaxCharge, damageChargeRemaining, false);        // init UI
    }

    // detonation charge countdown timer
    private void Update()
    {
        if (! turnCountdownRunning)
            return;

        tickTimeRemaining -= Time.deltaTime;

        // decrement damage 'charge' by 1
        if (tickTimeRemaining < 0)
        {
            damageChargeRemaining--;

            GameEvents.OnTurnChargeTick?.Invoke(CurrentPlayerMaxCharge, damageChargeRemaining, turnEnded);

            tickTimeRemaining = turnEnded ? turnEndedTickInterval : tickInterval;        // reset tick timer

            // start new turn if damageChargeRemaining reaches zero
            if (damageChargeRemaining <= 0)
            {
                damageChargeRemaining = 0;
                turnCountdownRunning = false;

                GameEvents.OnRaycastHit?.Invoke(null, null);                // deselect blocks and runes

                currentTurnPlayer.SetExtraHits(0);                          // end of turn - reset extra hits
                GameEvents.OnTurnEnd?.Invoke(currentTurnPlayer, true);
            }
        }
    }

    // on stack built
    // spawn new StackPlayer and link Stack to it and vice versa
    private void SpawnStackPlayer(Stack stack)
    {
        StackPlayer newPlayer = Instantiate(playerPrefab, transform);

        AllStackPlayers.Add(newPlayer);
        newPlayer.name = $"Player {AllStackPlayers.Count}";     // gameobject name - not displayed in game

        // assign Stack and colour to new StackPlayer
        int playerIndex = AllStackPlayers.Count - 1;
        newPlayer.AssignStack(stack, playerIndex, playerColours[playerIndex]);

        // assign new player to stack
        stack.AssignPlayer(newPlayer);
    }

    // new turn player set - start turn timer
    private void OnTurnChanged(StackPlayer newTurnPlayer)
    {
        //if (AllStacksOut)           // all players have no max charge or demolished stacks
        //{
        //    turnCountdownRunning = false;
        //    damageChargeRemaining = 0;

        //    if (gameOverAudio != null)
        //        AudioSource.PlayClipAtPoint(gameOverAudio, transform.position);

        //    GameEvents.OnGameOver?.Invoke();
        //    return;
        //}

        turnEnded = false;
        detonationCount = 0;
        damageChargeRemaining = CurrentPlayerMaxCharge;
        tickTimeRemaining = tickInterval;
        turnCountdownRunning = true;

        GameEvents.OnTurnChargeTick?.Invoke(CurrentPlayerMaxCharge, damageChargeRemaining, false);
    }

    private async void OnTurnEnd(StackPlayer player, bool timedOut)
    {
        turnEnded = true;

        if (timedOut)
        {
            await Task.Delay((int)(timeOutTurnPause * 1000f));      // pause before turn start (event timing)
            GameEvents.OnNextPlayerTurn?.Invoke();                  // set next player and start turn for all players
        }
        else      // ie. after detonation - allow a short time defore timeout (in case 'end turn' not clicked)
        {
            tickTimeRemaining = tickInterval;        // reset tick timer
            damageChargeRemaining = resetAfterDetonation;

            GameEvents.OnTurnChargeTick?.Invoke(CurrentPlayerMaxCharge, damageChargeRemaining, true);

            currentTurnPlayer.SetExtraHits(0);                   // end of turn - reset extra hits  

            await Task.Delay((int)(enablePhysicsDelay * 1000));

            if (currentStack != null)
                GameEvents.OnEnablePhysics?.Invoke(true, currentStack);
        }
    }

    // called by OnTurnStart - ie. local player whose turn has just started
    // relays currentTurnPlayer to all netcode players
    // to update turn 'status' for all players
    private void OnNextPlayerTurn()
    {
        if (AllStackPlayers == null || AllStackPlayers.Count == 0)
        {
            Debug.LogError("NextStackPlayer: no Players!!");
            return;
        }

        GameEvents.OnRaycastHit(null, null);        // deselect everything (to make sure)

        if (LastStackStanding)           // all players but one have no max charge or demolished stacks
        {
            turnCountdownRunning = false;
            damageChargeRemaining = 0;

            if (gameOverAudio != null)
                AudioSource.PlayClipAtPoint(gameOverAudio, transform.position);

            GameEvents.OnGameOver?.Invoke(currentTurnPlayer);
            return;
        }

        if (currentTurnPlayer == null)
        {
            currentTurnPlayer = allStacks[0].StackPlayer;
        }
        else
        {
            int currentPlayerIndex = CurrentPlayerTurnIndex;

            currentPlayerIndex++;
            if (currentPlayerIndex >= allStacks.Count)       // loop round to first again
                currentPlayerIndex = 0;

            currentTurnPlayer = allStacks[currentPlayerIndex].StackPlayer;
        }

        // skip player if has no charge or stack is demolished!
        if (currentTurnPlayer.Data.MaxCharge <= 0 || currentTurnPlayer.PlayerStack.StackDemolished)
        {
            OnNextPlayerTurn();       // recursive
            return;
        }

        // update 'turn status' and start turn for all players
        NetworkInterface.Instance.SetPlayerTurns(currentTurnPlayer);
    }

    // detonator button button clicked
    // use up an extra hit if not first hit
    // end turn if no extra hits left
    private void OnDetonate(float damagePerBlock, Rune hitRune)
    {
        // don't use extra hit if just added one by activating an ExtraHit rune!
        // Note: player's extra hits incremented (OnActivateRune) before OnDetonate event
        bool extraHitRune = hitRune != null && hitRune.Data.Power == RuneData.RunePower.ExtraHit && hitRune.RuneValue > 0;

        if (!extraHitRune)
        {
            detonationCount++;      // per turn

            if (currentTurnPlayer.HasExtraHits && detonationCount > 1)       // use up an extra hit (first hit free)
                currentTurnPlayer.UseExtraHit();
        }

        if (!currentTurnPlayer.HasExtraHits)
            GameEvents.OnTurnEnd?.Invoke(currentTurnPlayer, false);
    }

    private void OnStackDemolished(Stack stack)
    {
        int rank = AllStackPlayers.Count - DemolishedStackCount + 1;
        GameEvents.OnStackRank?.Invoke(stack, rank);
    }

    private void OnManualStackSelect(Stack selectedStack)
    {
        if (selectedStack != currentStack)
        {
            GameEvents.OnCameraOrbitStack?.Invoke(selectedStack);   // see below
            GameEvents.OnRaycastHit?.Invoke(null, null);            // deselect blocks and runes
        }
    }

    private void OnCameraOrbitStack(Stack stack)
    {
        if (stack != currentStack && stackChangedAudio != null)
            AudioSource.PlayClipAtPoint(stackChangedAudio, stack.transform.position);

        currentStack = stack;
    }

    private void OnStackBuilt(Stack newStack)
    {
        // instantiate new player and assign to newStack
        SpawnStackPlayer(newStack);
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        allStacks = stacks; 
        turnCountdownRunning = false;
    }

    public StackPlayer SetCurrentTurnPlayer(StackPlayer currentTurnPlayer)
    {
        this.currentTurnPlayer = currentTurnPlayer;
        return currentTurnPlayer;
    }

    // stack detonation audio

    private void OnDetonatorActivated(Stack stackToDetonate)
    {
        if (detonatorPlungerAudio != null)
            AudioSource.PlayClipAtPoint(detonatorPlungerAudio, stackToDetonate != null ? stackToDetonate.transform.position : transform.position);
    }

    private void OnDetonatorDown(Stack stackToDetonate, bool gameStart)
    {
        if (detonatorDownAudio != null)
            AudioSource.PlayClipAtPoint(detonatorDownAudio, stackToDetonate != null ? stackToDetonate.transform.position : transform.position);
    }
}
