using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class MusicMuteUI : MonoBehaviour
{
    [SerializeField] private Image MusicMuted;
    [SerializeField] private Image MusicUnmuted;

    private Button muteButton;

    private void Awake()
    {
        muteButton = GetComponent<Button>();
    }

    private void OnEnable()
    {
        GameEvents.OnMusicMuted += OnMusicMuted;
        muteButton.onClick.AddListener(MuteButtonClicked);
    }

    private void OnDisable()
    {
        GameEvents.OnMusicMuted -= OnMusicMuted;
        muteButton.onClick.RemoveListener(MuteButtonClicked);
    }

    private void MuteButtonClicked()
    {
        GameEvents.OnToggleMusicMute?.Invoke();
    }

    private void OnMusicMuted(bool muted)
    {
        MusicMuted.enabled = muted;
        MusicUnmuted.enabled = !muted;
    }
}
