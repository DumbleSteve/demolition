using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.UI;
//using System.Threading.Tasks;


public class AuthenticateUI : MonoBehaviour
{
    [SerializeField] private Image authenticatePanel;

    [SerializeField] private TMP_InputField playerNameInput;
    [SerializeField] private Button authenticateButton;

    [SerializeField] private TextMeshProUGUI errorText;      // player name problems
    //private float errorTime = 3f;

    private Vector2 startScale;
    private float scaleUpDelay = 0.3f;              // after detonator activated
    private float scaleUpTime = 0.15f;
    private float scaleUpMax = 1.4f;

    private const int MaxNameLength = 12;           // to fit in UI

    private string playerName;

    private void Awake()
    {
        startScale = authenticatePanel.transform.localScale;
        authenticatePanel.transform.localScale = Vector2.zero;
    }

    private void OnEnable()
    {
        GameEvents.OnAuthenticateError += OnAuthenticateError;           // eg. player name problems

        authenticateButton.onClick.AddListener(AuthenticateClicked);
        playerNameInput.onValueChanged.AddListener(PlayerNameChanged);
    }

    private void OnDisable()
    {
        GameEvents.OnAuthenticateError -= OnAuthenticateError;

        authenticateButton.onClick.RemoveListener(AuthenticateClicked);
        playerNameInput.onValueChanged.RemoveListener(PlayerNameChanged);
    }

    private void OnAuthenticateError(string error)
    {
        errorText.text = error;
        //ShowError(error);
    }

    private void PlayerNameChanged(string newName)
    {
        errorText.text = "";

        // restrict length of name
        if (newName.Length > MaxNameLength)
            newName = playerNameInput.text = newName[..MaxNameLength];

        playerName = newName;
        authenticateButton.interactable = playerName != "";
    }

    private void AuthenticateClicked()
    {
        if (string.IsNullOrEmpty(playerName))
        {
            errorText.text = "Please enter your Player Name";
            //ShowError("Please enter your Player Name");
            return;
        }

        if (LobbyManager.Instance.GetLobbyPlayerByName(playerName) != null)
        {
            errorText.text = $"Another Player is '{playerName}'";
            //ShowError("Please enter your Player Name");
            return;
        }

        errorText.text = "";
        authenticateButton.interactable = false;
        LobbyManager.Instance.Authenticate(playerName);
    }

    //private async void ShowError(string error)
    //{
    //    errorText.text = error;
    //    await Task.Delay((int)(errorTime * 1000f));
    //    errorText.text = "";
    //}

    public void Hide()
    {
        authenticatePanel.gameObject.SetActive(false);
    }

    public void Show()
    {
        errorText.text = "";
        playerNameInput.interactable = CheckInternetConnection();      // sets errorText if no internet
        authenticateButton.interactable = false;        // until player name entered

        authenticatePanel.gameObject.SetActive(true);

        LeanTween.scale(authenticatePanel.gameObject, startScale * scaleUpMax, scaleUpTime)
                    .setDelay(scaleUpDelay)
                    .setEaseOutCirc()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(authenticatePanel.gameObject, startScale, scaleUpTime * 0.5f)      // fast recoil
                                    .setDelay(scaleUpDelay)
                                    .setEaseInCirc();
                    });
    }


    // TODO: regular poll?  retry button??  move to LobbyManager refresh poll?
    private bool CheckInternetConnection()
    {
        errorText.text = "";

        // check if the device can reach the internet
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            errorText.text = "No Internet";
            return false;
        }

        // check if the device can reach the internet via a carrier data network
        if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            // ??
        }
        // check if the device can reach the internet via a LAN
        else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            // ??
        }

        return true;
    }
}