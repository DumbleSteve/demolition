using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// handles switching between lobby panels

public class LobbyUI : MonoBehaviour
{
    [SerializeField] private Image background;                  // semi-transparent black
    [SerializeField] private AuthenticateUI authenticateUI;     // input player name
    [SerializeField] private LobbyListUI lobbyListUI;           // list of available lobbies
    [SerializeField] private LobbyPlayersUI lobbyPlayersUI;     // players in joined lobby
    [SerializeField] private LobbyCreateUI lobbyCreateUI;       // input lobby name, visibility, max players amd game mode
    [SerializeField] private Image gamePanel;                   // name, credits, etc

    [SerializeField] private TextMeshProUGUI playerNameText;     
    [SerializeField] private TextMeshProUGUI debugText;

    private void Start()
    {
        background.gameObject.SetActive(true);
        gamePanel.gameObject.SetActive(true);

        playerNameText.text = "";
        playerNameText.gameObject.SetActive(true);

        authenticateUI.Hide();      // shown on detonator down!
        lobbyListUI.Hide();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    private void OnEnable()
    {
        GameEvents.OnDetonatorDown += OnDetonatorDown;                  // start of game - show authenticate panel
        GameEvents.OnAuthenticated += OnAuthenticated;

        GameEvents.OnCreateLobby += OnCreateLobby;
        GameEvents.OnJoinedLobby += OnJoinedLobby;
        GameEvents.OnLeftLobby += OnLeftLobby;
        GameEvents.OnKickedFromLobby += OnKickedFromLobby;

        GameEvents.OnLobbyStartGame += OnLobbyStartGame;
    }

    private void OnDisable()
    {
        GameEvents.OnDetonatorDown -= OnDetonatorDown;
        GameEvents.OnAuthenticated -= OnAuthenticated;

        GameEvents.OnCreateLobby -= OnCreateLobby;
        GameEvents.OnJoinedLobby -= OnJoinedLobby;
        GameEvents.OnLeftLobby -= OnLeftLobby;
        GameEvents.OnKickedFromLobby -= OnKickedFromLobby;

        GameEvents.OnLobbyStartGame -= OnLobbyStartGame;
    }

    // show sign-in
    private void OnDetonatorDown(Stack stackToDetonate, bool gameStart)
    {
        if (gameStart)
            authenticateUI.Show();
    }

    // show list of lobbies
    private void OnAuthenticated(string playerName, string playerId)
    {
        playerNameText.text = playerName;

        authenticateUI.Hide();
        lobbyListUI.Show();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    // show UI to set up new lobby
    private void OnCreateLobby()
    {
        authenticateUI.Hide();
        lobbyListUI.Hide();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Show();
    }

    // show list of lobbies
    private void OnLeftLobby()
    {
        authenticateUI.Hide();
        lobbyListUI.Show();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    // show list of lobbies
    private void OnKickedFromLobby()
    {
        authenticateUI.Hide();
        lobbyListUI.Show();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();
    }

    // show lobby players
    private void OnJoinedLobby(Lobby joinedLobby, bool isLobbyHost, string playerName)
    {
        authenticateUI.Hide();
        lobbyListUI.Hide();
        lobbyPlayersUI.Show();
        lobbyCreateUI.Hide();
    }

    // hide everything when game started
    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        authenticateUI.Hide();
        lobbyListUI.Hide();
        lobbyPlayersUI.Hide();
        lobbyCreateUI.Hide();

        background.gameObject.SetActive(false);
        gamePanel.gameObject.SetActive(false);
    }
}
