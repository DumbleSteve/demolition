using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;

// Assumes RelayManager used directly by LobbyManager to create/join relays
// as players start the game and leave the lobby
//
// LobbyManager authenticates and signs in, so no need to do it again here

public class RelayManager : MonoBehaviour
{
    public static RelayManager Instance { get; private set; }       // singleton

    private void Awake()
    {
        Instance = this;            // singleton
    }

    // start host
    public async Task<string> CreateRelay()
    {
        try
        {
            // create an allocation
            // relay decides best region
            Allocation allocation = await RelayService.Instance.CreateAllocationAsync(LobbyManager.PlayerLimit - 1);     // max connections does not include host!

            // get the allocation's join code (for lobby to use)
            string joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

            // must open an NGO connection for relay to work (UDP or DTLS connection type)
            RelayServerData relayServerData = new(allocation, "dtls");
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayServerData);
            NetworkManager.Singleton.StartHost();

            return joinCode;
        }
        catch (RelayServiceException e)
        {
            Debug.LogError($"CreateRelay: {e}");
            return null;
        }
    }

    // start client
    public async Task JoinRelay(string joinCode)
    {
        try
        {
            JoinAllocation joinAllocation = await RelayService.Instance.JoinAllocationAsync(joinCode);

            // must open an NGO connection for relay to work (UDP or DTLS connection type)
            RelayServerData relayServerData = new(joinAllocation, "dtls");
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(relayServerData);
            NetworkManager.Singleton.StartClient();
        }
        catch (RelayServiceException e)
        {
            Debug.LogError($"JoinRelay: {e}");
        }
    }
}
