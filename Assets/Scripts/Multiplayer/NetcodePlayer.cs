using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using System.Linq;


public class NetcodePlayer : NetworkBehaviour
{
    //private NetworkVariable<int> networkInt;
    //private NetworkVariable<float> networkFloat;
    //private NetworkVariable<bool> networkBool;

    // create an INetworkSerializable struct that can be used as a variable that is synced across the network
    // value types only!
    //private NetworkVariable<MyCustomData> randomNetworkData = new NetworkVariable<MyCustomData>(
    //    new MyCustomData
    //    {
    //        _int = 56,
    //        _bool = true,
    //        _message = "",
    //    }, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);       // server can always write

    public NetworkVariable<FixedString32Bytes> PlayerName { get; private set; } = new("", NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);     // set by local player on authentication
    public bool PlayerNameSet { get; private set; }

    public LobbyPlayerData LobbyPlayerData { get; private set; }          // from lobby

    // each NetcodePlayer will control one or more Stacks via a StackPlayer
    // each Stack has a single StackPlayer and vice versa
    public List<StackPlayer> StackPlayers { get; private set; } = new();

    private List<StackBlock> allStackBlocks = new();    // in all stacks
    private StackBlock GetBlockById(int id) => allStackBlocks.FirstOrDefault(x => x.BlockData.blockId == id);

    private List<Rune> allActiveRunes = new();                // in all stacks
    private Rune GetRuneByBlockId(int blockId) => blockId >= 0 ? allActiveRunes.FirstOrDefault(x => x.BlockId == blockId) : null;


    // NOTE: use this instead of Start or Awake for NetworkBehaviours!
    public override void OnNetworkSpawn()
    {
        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;        // to start first turn!
        GameEvents.OnRuneSpawned += OnRuneSpawned;              // cache for lookup by block id
        GameEvents.OnRuneExpired += OnRuneExpired;

        PlayerName.OnValueChanged += PlayerNameChanged;         // game starts when all player names have been set across network

        //if (IsHost)
        //{
        //    NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        //    NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;
        //}

        // cache player and set/sync all player names across network
        NetworkInterface.Instance.RegisterNetcodePlayer(this);
        base.OnNetworkSpawn();
    }

    public override void OnNetworkDespawn()
    {
        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnRuneSpawned -= OnRuneSpawned;
        GameEvents.OnRuneExpired -= OnRuneExpired;

        PlayerName.OnValueChanged -= PlayerNameChanged;
        NetworkInterface.Instance.RemoveNetcodePlayer(OwnerClientId);      // eg. add to UI / reactivate buttons etc.
    }


    private void PlayerNameChanged(FixedString32Bytes previousValue, FixedString32Bytes newValue)
    {
        PlayerNameSet = !string.IsNullOrEmpty(newValue.ToString());
        NetworkInterface.Instance.StartIfAllPlayerNamesSet();      // netcode and stack players linked, stacks built etc. when all player names have been set
    }

    // called whe all netcode players have been spawned
    public void SetPlayerName()
    {
        if (IsOwner)        // PlayerName network variable writeable by Owner
            PlayerName.Value = LobbyManager.Instance.LocalPlayerName;
    }

    // called when all NetcodePlayers have been spawned
    // save player data from lobby
    public void SetLobbyPlayerData(LobbyPlayerData lobbyPlayerData)
    {
        LobbyPlayerData = lobbyPlayerData;
    }

    // called when all NetcodePlayers have been spawned and all Stacks built
    // attach StackPlayer (and therefore Stack) to this NetcodePlayer
    // and vice versa
    public void LinkToStackPlayer(StackPlayer stackPlayer)
    {
        StackPlayers.Add(stackPlayer);
        stackPlayer.AssignNetcodePlayer(this);
    }


    #region player turn synchronisation

    // host starts the turn machine!
    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        if (IsOwner)
        {
            CreateBlockList(stacks);      // all blocks in all stacks

            if (IsHost)
            {
                GameEvents.OnNextPlayerTurn?.Invoke();      // set first player and start turn for all players
            }
        }
    }

    // called by TurnManager for all players at turn start
    // when the next turn player is set ny NetworkTurnManager
    public void SetPlayerTurn(StackPlayer currentTurnPlayer)
    {
        PlayerTurnServerRPC(currentTurnPlayer.NetcodePlayerId, currentTurnPlayer.PlayerStack.StackName);
    }

    // client to server
    // this players turn - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void PlayerTurnServerRPC(ulong turnOwnerClientId, string stackName)
    {
        PlayerTurnClientRPC(turnOwnerClientId, stackName);                    // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void PlayerTurnClientRPC(ulong turnOwnerClientId, string stackName)
    {
        if (!IsOwner)
            return;

        bool thisPlayersTurn = OwnerClientId == turnOwnerClientId;
        NetworkInterface.Instance.NetworkStartTurn(turnOwnerClientId, stackName, thisPlayersTurn);    // 'your turn' UI, enable/disable raycast, etc
    }

#endregion


#region selected blocks/rune synchronisation

    // OnRaycastEnd - to select all raycastblocks/rune on all clients for potential detonation
    // called for all players
    public void SelectRaycastBlocks(List<StackBlock> hitBlocks, Rune hitRune)
    {
        if ((hitBlocks == null || hitBlocks.Count == 0) && hitRune == null)
        {
            SelectBlocksServerRPC(null, -1);
            return;
        }

        // convert hitBlocks to array of Ids
        int[] hitBlockIds = new int[hitBlocks.Count];

        for (int i = 0; i < hitBlocks.Count; i++)
        {
            hitBlockIds[i] = hitBlocks[i].BlockData.blockId;
        }

        SelectBlocksServerRPC(hitBlockIds, hitRune != null ? hitRune.BlockId : -1);
    }

    // client to server - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void SelectBlocksServerRPC(int[] hitBlockIds, int hitRuneId)
    {
        SelectBlocksClientRPC(hitBlockIds, hitRuneId);                    // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void SelectBlocksClientRPC(int[] hitBlockIds, int hitRuneId)
    {
        if (!IsOwner)
            return;

        if (hitBlockIds == null && hitRuneId == -1)
        {
            GameEvents.OnRaycastHit?.Invoke(null, null);
            return;
        }

        // convert hitBlockIds back to a list of StackBlocks for OnRaycastHit
        List<StackBlock> hitBlocks = new();

        foreach (var blockId in hitBlockIds)
        {
            var block = GetBlockById(blockId);

            if (block != null)
                hitBlocks.Add(block);
        }

        // convert rune id back to Rune for OnRaycastHit
        Rune hitRune = GetRuneByBlockId(hitRuneId);

        GameEvents.OnRaycastHit?.Invoke(((hitBlocks != null && hitBlocks.Count > 0) ? hitBlocks : null), hitRune);
    }

    // cache all spawned runes for lookup by block id
    private void OnRuneSpawned(Rune newRune)
    {
        if (IsOwner)
            allActiveRunes.Add(newRune);
    }

    private void OnRuneExpired(Rune rune)
    {
        if (IsOwner)
            allActiveRunes.Remove(rune);
    }

    #endregion


    #region block detonation!

    // on detonate buttonClicked -> detonate all selected blocks
    // called for all players
    public void DetonateBlocks(float damagePerBlock) 
    {
        DetonateBlocksServerRPC(damagePerBlock);
    }

    // client to server - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void DetonateBlocksServerRPC(float damagePerBlock) 
    {
        DetonateBlocksClientRPC(damagePerBlock);               // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void DetonateBlocksClientRPC(float damagePerBlock)
    {
        if (!IsOwner)
            return;

        GameEvents.OnDetonate?.Invoke(damagePerBlock, null);
        GameEvents.OnRaycastHit?.Invoke(null, null);            // deselect everything
    }

    #endregion


    #region block demolition / rune spawning

    // on block demolition, to spawn same rune across all players
    public void DemolishBlock(int blockId, string runeName)
    {
        DemolishBlockServerRPC(blockId, runeName);
    }

    // client to server - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void DemolishBlockServerRPC(int blockId, string runeName)
    {
        DemolishBlockClientRPC(blockId, runeName);                  // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void DemolishBlockClientRPC(int blockId, string runeName)
    {
        if (!IsOwner)
            return;

        GameEvents.OnDemolishBlock?.Invoke(blockId, runeName);     // activate rune before OnDetonate, which determines if turn ends (extra hits or not)
    }

    #endregion


    #region rune activation!

    // on detonate buttonClicked -> activate selected rune
    // called for all players
    public void ActivateRune()
    {
        ActivateRuneServerRPC();
    }

    // client to server - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void ActivateRuneServerRPC()
    {
        ActivateRuneClientRPC();                  // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void ActivateRuneClientRPC()
    {
        if (!IsOwner)
            return;

         GameEvents.OnActivateRune?.Invoke();     // activate rune before OnDetonate, which determines if turn ends (extra hits or not)
    }

    #endregion


    #region stack demolition!

    public void DemolishStack(string stackName)
    {
        DemolishStackServerRPC(stackName);
    }

    // client to server - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void DemolishStackServerRPC(string stackName)
    {
         DemolishStackClientRPC(stackName);                // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void DemolishStackClientRPC(string stackName)
    {
        if (!IsOwner)
            return;

        GameEvents.OnDemolishStack?.Invoke(stackName);
    }

    #endregion


    #region restore current stack pose (on start raycasting stack)

    public void RestoreStackPose()
    {
        RestoreStackPoseServerRPC();
    }

    // client to server - call to all clients (of this player)
    [ServerRpc(RequireOwnership = false)]
    private void RestoreStackPoseServerRPC()
    {
        RestoreStackPoseClientRPC();                // server to all clients
    }

    // server to all clients (of this player)
    [ClientRpc]
    private void RestoreStackPoseClientRPC()
    {
         if (!IsOwner)
            return;

        GameEvents.OnRestoreStackPose?.Invoke();
    }

    #endregion

    // once only - on all stacks built
    private void CreateBlockList(List<Stack> allStacks)
    {
        allStackBlocks = new();

        foreach (var stack in allStacks)
        {
            foreach (var level in stack.StackLevels)
            {
                foreach (var row in level.LevelRows)
                {
                    foreach (var block in row.RowBlocks)
                    {
                        allStackBlocks.Add(block);
                    }
                }
            }
        }
    }

    //private void OnClientConnected(ulong clientID)
    //{
    //}

    //// response to NetworkManager client disconnected event
    //// if host left, disconnect all clients
    //private void OnClientDisconnected(ulong clientID)
    //{
    //}
}
