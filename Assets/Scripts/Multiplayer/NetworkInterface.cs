using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Unity.Services.Lobbies.Models;


// handles networking syncing - via ClientRPCs
// in conjunction with TurnManager singleton
// typically simply 'forwards' key network 'events' onto all clients via local events
//
// contains a list of NetcodePlayers, populated as each player is spawned
public class NetworkInterface : MonoBehaviour
{
    public static NetworkInterface Instance { get; private set; }       // singleton

    private StackPlayer StackPlayerByStackName(string stackName) => TurnManager.Instance.AllStackPlayers.FirstOrDefault(x => x.PlayerStack.StackName == stackName);

    [SerializeField] private RuneLibrary runeLibrary;       // set in inspector
    [SerializeField] private AudioClip nextPlayerAudio;

    private readonly List<NetcodePlayer> netcodePlayers = new(); // add as spawned - host (as creator of relay) will be first, clients in random order (ie. as spawned)
    public List<NetcodePlayer> OrderedNetcodePlayers => netcodePlayers.OrderBy(p => p.OwnerClientId).ToList();
    private bool AllNetcodePlayersSpawned => netcodePlayers.Count == LobbyManager.Instance.LobbyMaxPlayers;

    private int PlayerNameCount => netcodePlayers.Count(p => p.PlayerNameSet);
    private bool AllNetcodePlayerNamesSet => PlayerNameCount == LobbyManager.Instance.LobbyMaxPlayers;

    public bool IsLocalPlayersTurn { get; private set; }            // detonation etc. disabled if not local player's turn


    private void Awake()
    {
        Instance = this;        // singleton
    }

    private void OnEnable()
    {
        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;
        GameEvents.OnRaycastEnd += OnRaycastEnd;                    // right-mouse up
    }

    private void OnDisable()
    {
        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnRaycastEnd -= OnRaycastEnd;
    }

    public void SetPlayerTurns(StackPlayer currentTurnPlayer)
    {
        // update 'turn status' for all players (any order)
        foreach (var player in netcodePlayers)
        {
            player.SetPlayerTurn(currentTurnPlayer);      // ServerRPC -> ClientRPC (OnNetworkTurnChanged callback event)
        }
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        // all NetcodePlayers in game and all Stacks / StackPlayers constructed, so link them
        LinkStackNetcodePlayers();

        // Host NetcodePlayer starts first turn
    }

    // select blocks / rune for detonation for all players
    private void OnRaycastEnd(List<StackBlock> hitBlocks, Rune hitRune)
    {
        // update selected blocks for all players (any order)
        foreach (var player in netcodePlayers)
        {
            player.SelectRaycastBlocks(hitBlocks, hitRune);      // ServerRPC -> ClientRPC
        }
    }

    public void NetworkDetonateBlocks(float damagePerBlock)
    {
        // detonate blocks for all players (any order)
        foreach (var player in netcodePlayers)
        {
            player.DetonateBlocks(damagePerBlock);      // ServerRPC -> ClientRPC
        }
    }

    public void NetworkDemolishBlock(int blockId, bool spawnRune)
    {
        if (!IsLocalPlayersTurn)
            return;

        // spawn (same) random rune for all players
        var runeName = spawnRune ? runeLibrary.RandomRune : "";

        //if (string.IsNullOrEmpty(runeName))             // no (random) rune for this block!
        //    return;

        foreach (var player in netcodePlayers)
        {
            player.DemolishBlock(blockId, runeName);      // ServerRPC -> ClientRPC (Owner only)
        }
    }

    public void NetworkActivateRune()
    {
        // activate selected rune for all players
        foreach (var player in netcodePlayers)
        {
            player.ActivateRune();      // ServerRPC -> ClientRPC (Owner only)
        }
    }

    public void NetworkDemolishStack(string stackName)
    {
        // demolish stack for all players
        foreach (var player in netcodePlayers)
        {
            player.DemolishStack(stackName);      // ServerRPC -> ClientRPC (Owner only)
        }
    }

    public void NetworkRestoreStackPose()
    {
        // retore current stack pose (and disable physics) for all players (if raycast hit blocks or rune)
        foreach (var player in netcodePlayers)
        {
            player.RestoreStackPose();      // ServerRPC -> ClientRPC (Owner only)
        }
    }

    // called by NetcodePlayer client RPC
    // fire local events to start turn and update 'your turn' status of all players
    public void NetworkStartTurn(ulong newTurnPlayerId, string stackName, bool isPlayersTurn)
    {
        //Debug.Log($"<color=lime>NetworkTurnManager.OnNetworkStartTurn newTurnPlayerId {newTurnPlayerId} stackName {stackName} isPlayersTurn {isPlayersTurn}</color>");
        GameEvents.OnTurnStart?.Invoke();

        // set current stack player
        var currentTurnPlayer = TurnManager.Instance.SetCurrentTurnPlayer(StackPlayerByStackName(stackName));
        GameEvents.OnTurnChanged?.Invoke(currentTurnPlayer);

        IsLocalPlayersTurn = isPlayersTurn;
        //bool onePlayerLeft = netcodePlayers.Count == 1 || TurnManager.Instance.LastStackStanding;
        GameEvents.OnIsLocalPlayersTurn?.Invoke(isPlayersTurn, netcodePlayers.Count);

        // unfreeze player's stack
        GameEvents.OnFreezeStack?.Invoke(currentTurnPlayer.PlayerStack, false);

        // focus on new player's stack, regardless of which is currently selected
        GameEvents.OnCameraOrbitStack?.Invoke(currentTurnPlayer.PlayerStack);

        if (nextPlayerAudio != null)
            AudioSource.PlayClipAtPoint(nextPlayerAudio, currentTurnPlayer.PlayerStack.transform.position);
    }


    #region netcode players join/leave

    // cache newly spawned NetcodePlayer and set all player names (network variable)
    // when all NetcodePlayers are in-game
    public void RegisterNetcodePlayer(NetcodePlayer netcodePlayer)
    {
        netcodePlayers.Add(netcodePlayer);
        //GameEvents.OnNetworkPlayerEnteredGame?.Invoke(netcodePlayer, netcodePlayers, LobbyManager.Instance.LobbyMaxPlayers);        // TODO: remove?

        // if all NetcodePlayers are in game, set all player names before assigning lobby player data and then building stacks!
        if (AllNetcodePlayersSpawned)
        {
            // set all player names
            foreach (var player in netcodePlayers)
            {
                if (player.IsOwner)
                    player.SetPlayerName();     // owner sets player name network variable (to sync across network)
            }
        }
    }

    // once all player names have been set, use names to get lobby player data
    // and build stacks -> start game
    public void StartIfAllPlayerNamesSet()
    {
        if (AllNetcodePlayerNamesSet)
        {
            List<LobbyPlayerData> lobbyPlayers = LinkLobbyNetcodePlayers();
            GameEvents.OnConstructStackBuildData?.Invoke(lobbyPlayers);         // construct stack build data (using player names)
            GameEvents.OnReadyToBuildStacks?.Invoke();
        }
    }

    #endregion

    // when all NetcodePlayers are in the game and all player names have been set across network,
    // set player name, sprite and joined datetime for all netcode players
    private List<LobbyPlayerData> LinkLobbyNetcodePlayers()
    {
        if (!AllNetcodePlayersSpawned)      // shouldn't be here yet if not! (event sequence)
            return null;

        if (!AllNetcodePlayerNamesSet)      // shouldn't be here yet if not! (event sequence)
            return null;

        List<LobbyPlayerData> lobbyPlayers = new();
        List<NetcodePlayer> orderedPlayers = OrderedNetcodePlayers;       // in the order the players NetcodePlayers were spawned (ie. when they picked up join code)

        for (int playerIndex = 0; playerIndex < LobbyManager.Instance.LobbyMaxPlayers; playerIndex++)
        {
            NetcodePlayer netcodePlayer = orderedPlayers[playerIndex];

            // matching by lobby player name, set character sprite and joined time when new netcode player is spawned
            string netcodePlayerName = netcodePlayer.PlayerName.Value.ToString();     // network variable
            Player lobbyPlayer = LobbyManager.Instance.GetLobbyPlayerByName(netcodePlayerName);

            //Debug.Log($"<color=orange>LinkLobbyNetcodePlayers: playerName '{netcodePlayerName}' {(lobbyPlayer != null ? "Found player!" : "Player not found!!")}</color>");

            //string playerName = LobbyManager.Instance.LobbyPlayerName(lobbyPlayer);
            Sprite playerCharacter = LobbyManager.Instance.LobbyPlayerCharacter(lobbyPlayer);

            LobbyPlayerData lobbyPlayerData = new() { PlayerName = netcodePlayerName, PlayerCharacter = playerCharacter, Joined = lobbyPlayer.Joined };
            lobbyPlayers.Add(lobbyPlayerData);
            netcodePlayer.SetLobbyPlayerData(lobbyPlayerData);
        }

        return lobbyPlayers;
    }

    // when all NetcodePlayers are in the game and all Stacks (and therefore StackPlayers) have been constructed
    // link each NetcodePlayer to its corresponding StackPlayer(s)
    // and each StackPlayer to its (single) NetcodePlayer
    private void LinkStackNetcodePlayers()
    {
        if (!AllNetcodePlayersSpawned)      // shouldn't be here yet if not! (event sequence)
            return;

        List<StackPlayer> orderedStackPlayers = TurnManager.Instance.OrderedStackPlayers;        // ordered by date/time joined lobby
        List<NetcodePlayer> orderedNetcodePlayers = OrderedNetcodePlayers;        // in order NetcodePlayers spawned (ie. when they picked up join code)

        // attach each StackPlayer (and therefore Stack) to its corresponding NetcodePlayer
        // there may be fewer players than stacks...
        for (int stackIndex = 0, playerIndex = 0; stackIndex < TurnManager.Instance.AllStackPlayers.Count; stackIndex++, playerIndex++)
        {
            if (playerIndex >= LobbyManager.Instance.LobbyMaxPlayers)      // cycle through players if fewer than stacks
                playerIndex = 0;

            NetcodePlayer netcodePlayer = orderedNetcodePlayers[playerIndex];
            StackPlayer stackPlayer = orderedStackPlayers[stackIndex];

            //Debug.Log($"<color=teal>LinkStackNetcodePlayers: stackPlayer '{stackPlayer.PlayerStack.StackName}'  OwnerClientId {netcodePlayer.OwnerClientId}</color>");
            netcodePlayer.LinkToStackPlayer(stackPlayer);       // 2-way link StackPlayer and NetcodePlayer
        }

        GameEvents.OnAllPlayersLinked?.Invoke();
    }

    public void RemoveNetcodePlayer(ulong ownerClientId)
    {
        NetcodePlayer player = netcodePlayers.FirstOrDefault(x => x.OwnerClientId == ownerClientId);

        if (player != null)
            netcodePlayers.Remove(player);
    }
}

