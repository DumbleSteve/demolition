
// data for each player (max charge etc)

[System.Serializable]
public class PlayerData
{
    public int PlayerIndex;

    public LobbyPlayerData LobbyPlayerData;         // from lobby

    public int MaxCharge = 240;         // 'charge' counts down every tickIntervalSeconds, resets at start of turn
    public int ExtraHits = 0;           // increased/decreased with runes, used up by block / rune detonation
}
