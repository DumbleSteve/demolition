using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class Detonator : MonoBehaviour
{
    [SerializeField] private Button detonateButton;
    [SerializeField] private Image detonatorBase;
    [SerializeField] private Image detonatorPlunger;

    [SerializeField] private Transform plungerDownPoint;
    [SerializeField] private bool detonateOnStart = false;

    [SerializeField] private Color detonatorDisabledColour = Color.grey;     // when button not interactable

    private bool activating = false;
    private bool plungerDown = true;                                // starts down until enabled
    private float detonatorPlungeTime = 0.35f;                      // plunger travel time
    private float detonateOnStartDelay = 0.2f;                      // pause before detonation on start

    private float detonatePulseInterval = 1.5f;                       // only when enabled
    private float detonatePulseTimer = 1.5f;                          // pulse immediately

    private Action onDetonate = null;                               // callback on detonation!
    private Stack stackToDetonate;

    private Pulser pulser;


    private void Awake()
    {
        pulser = GetComponent<Pulser>();
    }

    private async void Start()
    {
        if (detonateOnStart)
        {
            await Task.Delay((int)(detonateOnStartDelay * 1000f));
            plungerDown = false;
            ActivatePlunger(true, true, true);
        }
    }

    private void Update()
    {
        if (!plungerDown && detonateButton.interactable)
        {
            detonatePulseTimer += Time.deltaTime;

            if (detonatePulseTimer >= detonatePulseInterval)
            {
                Pulse();
                detonatePulseTimer = 0f;     // reset
            }
        }
    }

    private void OnEnable()
    {
        detonateButton.onClick.AddListener(DetonateClicked);
    }

    private void OnDisable()
    {
        detonateButton.onClick.RemoveListener(DetonateClicked);
    }

    // set callback function on detonation
    public void PrimeDetonator(Action detonateCallback, Stack stackToDetonate)
    {
        onDetonate = detonateCallback;
        this.stackToDetonate = stackToDetonate;

        ActivatePlunger(true, false, false);              // down to start, up when enabled, down on detonation
    }

    // plunger up if enabled
    public void EnableDetonator(bool enable)
    {
        detonateButton.interactable = enable;
        detonatorBase.color = detonatorPlunger.color = (enable ? Color.white : detonatorDisabledColour);

        if (enable && plungerDown)
        {
            ActivatePlunger(false, false, false);      // up - ready to detonate!
        }
    }

    private void DetonateClicked()
    {
        ActivatePlunger(true, true, false);
    }

    private void ActivatePlunger(bool down, bool detonate, bool gameStart)
    {
         if (activating || plungerDown == down)
            return;

        if (down && detonate)
            GameEvents.OnDetonatorActivated?.Invoke(stackToDetonate);

        plungerDown = down;
        activating = true;

        LeanTween.moveLocalY(detonatorPlunger.gameObject, (down ? plungerDownPoint.localPosition.y : 0f), detonatorPlungeTime)
                    .setEaseInOutCubic()
                    .setOnComplete(() =>
                    {
                        if (down && detonate)
                        {
                            Pulse();
                            EnableDetonator(false);

                            onDetonate?.Invoke();
                            GameEvents.OnDetonatorDown?.Invoke(stackToDetonate, gameStart);
                        }

                        activating = false;
                    });
    }

    private void Pulse()
    {
        if (pulser != null && detonateButton.interactable)
            pulser.Pulse(transform);
    }
}