using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;
using Unity.Services.Lobbies.Models;

// raycasts for blocks or runes in the currently
// active stack while right mouse button held down

public class BlockRaycaster : MonoBehaviour
{
    [SerializeField] private LayerMask blockLayers;     // for 'activating' blocks
    [SerializeField] private LayerMask runeLayers;      // for 'activating' runes

    [SerializeField] private AudioClip damageBlockDepthAudio;     // when damageBlockDepth changes

    // raycasting
    private Ray mouseRay;                               // ray from mouse screen position
    private float rayCastDistance = 2.5f;               // damage 'reach'
    private float mouseWheelDeltaThreshold = 0.5f;      // to increase/decrease raycast 'depth' (number of blocks)

    private float rayCastInterval = 0.05f;              // (seconds) so not every frame...
    private bool raycasting = false;                    // every rayCastInterval seconds

    // raycast hits
    private const int hitArraySize = maxDamageBlockDepth * 2;    // double size of hit array as hits are not in proximity order
    private RaycastHit[] rayCastHits = new RaycastHit[hitArraySize];

    private int extraRaycastHits = 0;                       // set by rune - reset at end of turn, set according to player at turn start

    private readonly List<StackBlock> hitBlocks = new();    // from raycast, ordered by proximity - blocks to take damage
    private Rune hitRune;                                   // from raycast

    // raycast block depth
    private const int maxDamageBlockDepth = 5;              // max number of blocks 'deep' into hitBlocks list to damage
    private const int minDamageBlockDepth = 1;              // min number of blocks 'deep' into hitBlocks list to damage
    private int damageBlockDepth = 1;                       // current number of blocks 'deep' into hitBlocks list to damage - set via mouse wheel

    private Stack currentStack;
    private bool stackPoseRestored = false;                 // on first raycast of block(s) in currentStack

    private bool gameStarted = false;                       // from lobby
    private bool turnEnded = true;                          // raycast for blocks once per turn

    private Camera mainCam;


    private void OnEnable()
    {
        GameEvents.OnLobbyStartGame += OnLobbyStartGame;      // via lobby

        GameEvents.OnTurnStart += OnTurnStart;      // next turn started
        GameEvents.OnTurnEnd += OnTurnEnd;          // on stack detonation or timeout
        GameEvents.OnTurnChanged += OnTurnChanged;        // fired by TurnManager.NextPlayer after OnTurnStart

        GameEvents.OnRestoreStackPose += OnRestoreStackPose;      
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;
    }

    private void OnDisable()
    {
        GameEvents.OnLobbyStartGame -= OnLobbyStartGame;

        GameEvents.OnTurnStart -= OnTurnStart;  
        GameEvents.OnTurnEnd -= OnTurnEnd;
        GameEvents.OnTurnChanged -= OnTurnChanged;

        GameEvents.OnRestoreStackPose -= OnRestoreStackPose;
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
    }

    private void Start()
    {
        // get Camera.main once only for efficiency
        mainCam = Camera.main;
        damageBlockDepth = 1;
        GameEvents.OnBlockHitDepthChanged?.Invoke(damageBlockDepth);     // initial value, for UI
    }

    // detect mouse inputs for raycasting
    private void Update()
    {
        if (!gameStarted)
            return;

        if (!NetworkInterface.Instance.IsLocalPlayersTurn)
            return;

        // right-mouse to raycast
        if (Input.GetMouseButtonDown(1))                    
        {
            if (currentStack != null && (currentStack.StackDemolished || currentStack.StackFrozen))
                return;

            StartRaycasting();
        }

        //if (Input.GetMouseButton(1))
        //{
        //    if (DoRaycast(Input.mousePosition))
        //    {
        //        // disable physics and restore blocks' pose if raycast hit blocks or rune
        //        GameEvents.OnEnablePhysics?.Invoke(false, currentStack);
        //        GameEvents.OnRestoreBlocks?.Invoke(currentStack, false);

        //        GetMouseWheelDelta();                             // block hit depth
        //    }
        //}

        if (Input.GetMouseButtonUp(1))                      // stop raycasting
        {
            raycasting = false;
            GameEvents.OnRaycastEnd?.Invoke(hitBlocks, hitRune);
        }
    }

    // loop until raycasting is false
    private async void StartRaycasting()
    {
        if (raycasting || !NetworkInterface.Instance.IsLocalPlayersTurn)
            return;

        raycasting = true;

        while (raycasting)
        {
            if (DoRaycast(Input.mousePosition) && !turnEnded)       // blocks or rune raycast hits
            {
                if (hitBlocks.Count > 0)
                    GetMouseWheelDelta();                             // block hit depth

                if (!stackPoseRestored)        // to prevent multiple pose restores!
                {
                    NetworkInterface.Instance.NetworkRestoreStackPose(); // disable physics and restore blocks' pose if raycast hit blocks or rune
                    stackPoseRestored = true;
                }
            }
 
            await Task.Delay((int)(rayCastInterval * 1000f));
        }
    }

    // raycast for target collider layer from mouse/touch screenPosition
    // make a list of blocks hit, according to damageBlockDepth
    private bool DoRaycast(Vector2 mousePosition)
    {
        mouseRay = mainCam.ScreenPointToRay(mousePosition);
        hitBlocks.Clear();
        hitRune = null;

        int blockHitCount = 0;
        Array.Clear(rayCastHits, 0, hitArraySize);

        if (! turnEnded)                                   // once per turn
        {
            // first raycast to detect hit blocks
            blockHitCount = Physics.RaycastNonAlloc(mouseRay, rayCastHits, rayCastDistance, blockLayers);

            if (blockHitCount > 0)
            {
                int numBlocksToHit = Mathf.Min(damageBlockDepth, blockHitCount);

                // sort all hits in order of ray hit distance
                List<RaycastHit> orderedByProximity = rayCastHits.OrderBy(hit => hit.distance).ToList();

                // make a list of StackBlocks to take damage
                // filter out any blocks that don't belong to currentStack!
                foreach (RaycastHit hit in orderedByProximity)
                {
                    if (hit.transform == null)      // may be 'empty' hit (from array.clear)
                        continue;

                    var hitBlock = hit.transform.GetComponent<StackBlock>();
                    if (hitBlock == null || !hitBlock.IsChildOfStack(currentStack))
                        continue;

                    hitBlocks.Add(hitBlock);

                    if (hitBlocks.Count == numBlocksToHit)
                        break;
                }
            }
        }

        // can always raycast for a rune, to see its properties - current stack only! 
        if (blockHitCount == 0)
        {
            if (Physics.Raycast(mouseRay, out RaycastHit hitInfo, rayCastDistance, runeLayers, QueryTriggerInteraction.Collide))
            {
                if (hitInfo.transform != null)
                {
                    hitRune = hitInfo.transform.GetComponent<Rune>();

                    // can't select a rune from another stack or if it's been activated already
                    if (hitRune.ParentStack != currentStack || hitRune.CurrentState == Rune.RuneState.Activated)
                        hitRune = null;
                }
            }
        }

        GameEvents.OnRaycastHit?.Invoke(hitBlocks.Count > 0 ? hitBlocks : null, hitRune);   // hilight blocks / rune

        return blockHitCount > 0 || hitRune != null;
    }

    // while raycasting blocks, detect mouse wheel scroll to change damageBlockDepth
    private void GetMouseWheelDelta()
    {
        if (raycasting && hitBlocks.Count > 0)          // hitting blocks!
        {
            float mouseWheelDelta = Input.mouseScrollDelta.y;

            //if (mouseWheelDelta != 0)
            //    Debug.Log($"GetMouseWheelDelta: mouseWheelDelta {mouseWheelDelta}  mouseWheelThreshold {mouseWheelDeltaThreshold}");

            if (Mathf.Abs(mouseWheelDelta) > mouseWheelDeltaThreshold)
            {
                var currentBlockDepth = damageBlockDepth;       // to detect a change in value

                if (mouseWheelDelta > mouseWheelDeltaThreshold)          // mouse wheel up
                {
                    damageBlockDepth--;
                }
                else if (mouseWheelDelta < mouseWheelDeltaThreshold)
                {
                    damageBlockDepth++;
                }

                damageBlockDepth = Mathf.Clamp(damageBlockDepth, minDamageBlockDepth, maxDamageBlockDepth);

                if (damageBlockDepth != currentBlockDepth)
                {
                    GameEvents.OnBlockHitDepthChanged?.Invoke(damageBlockDepth);

                    if (damageBlockDepthAudio != null)
                        AudioSource.PlayClipAtPoint(damageBlockDepthAudio, transform.position);
                }
            }
        }
    }

    private void OnTurnStart()
    {
        turnEnded = false;
    }

    private void OnTurnEnd(StackPlayer player, bool timedOut)
    {
        turnEnded = true;
        stackPoseRestored = false;
    }

    private void OnTurnChanged(StackPlayer currentPlayer)
    {
        extraRaycastHits = currentPlayer.Data.ExtraHits;        // decremented by each detonation
    }

    // disable physics and restore blocks' pose if raycast hit blocks or rune
    private void OnRestoreStackPose()
    {
        GameEvents.OnEnablePhysics?.Invoke(false, currentStack);
        GameEvents.OnRestoreBlocks?.Invoke(currentStack, false);
    }

    private void OnCameraOrbitStack(Stack stack)
    {
        currentStack = stack;
    }

    private void OnLobbyStartGame(LobbyPlayerData lobbyPlayerData)
    {
        gameStarted = true;
        //Debug.Log($"OnLobbyStartGame: {gameStarted}");
    }

    private void OnDrawGizmos()
    {
        if (!Input.GetMouseButton(1))
            return;

        Gizmos.color = Color.red;

        var rayStart = mouseRay.GetPoint(0);
        var rayEnd = rayStart + (mouseRay.direction * rayCastDistance);

        //Gizmos.DrawWireSphere(rayStart, sphereCastRadius);
        Gizmos.DrawLine(rayStart, rayEnd);
        //Gizmos.DrawWireSphere(rayEnd, sphereCastRadius);
    }
}