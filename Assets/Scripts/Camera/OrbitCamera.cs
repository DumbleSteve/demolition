using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;


// enables Cinemachine camera orbiting of a specific stack or the table
// only when left mouse is held down

[RequireComponent(typeof(CinemachineFreeLook))]

public class OrbitCamera : MonoBehaviour
{
    [SerializeField] private Transform Table;

    private CinemachineFreeLook orbitCamera;

    private string XAxisName = "Mouse X";
    private string YAxisName = "Mouse Y";

    private bool allStacksBuilt = false;

    //private Transform saveLookAt = null;        // saved during block raycast
    //private Transform saveFollow = null;     // saved during block raycast


    private void Awake()
    {
        orbitCamera = GetComponent<CinemachineFreeLook>();

        OnCameraOrbitTable();

        // disable orbiting (until left mouse is held down)
        orbitCamera.m_XAxis.m_InputAxisName = "";
        orbitCamera.m_YAxis.m_InputAxisName = "";
    }

    private void OnEnable()
    {
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;        // sets stack to look at / follow
        GameEvents.OnCameraOrbitTable += OnCameraOrbitTable;        // look at / follow table

        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;            // activates free look

        //GameEvents.OnRaycastBlocks += OnRaycastBlocks;              // look at first raycast hit block
    }

    private void OnDisable()
    {
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable -= OnCameraOrbitTable;

        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;

        //GameEvents.OnRaycastBlocks -= OnRaycastBlocks;              // restore look at
    }

    // only enable orbiting when left mouse button is held down
    private void Update()
    {
        if (!allStacksBuilt)
            return;

        if (Input.GetMouseButton(0))
        {
            orbitCamera.m_XAxis.m_InputAxisValue = Input.GetAxis(XAxisName);
            orbitCamera.m_YAxis.m_InputAxisValue = Input.GetAxis(YAxisName);
        }
        else
        {
            orbitCamera.m_XAxis.m_InputAxisValue = 0;
            orbitCamera.m_YAxis.m_InputAxisValue = 0;
        }
    }

    // set stack to orbit around (look at/follow)
    private void OnCameraOrbitStack(Stack stack)
    {
        if (stack == null)
            return;

        orbitCamera.LookAt = stack.FocalPoint;
        orbitCamera.Follow = stack.FocalPoint;
    }

    private void OnCameraOrbitTable()
    {
        if (Table == null)
            return;

        orbitCamera.LookAt = Table.transform;
        orbitCamera.Follow = Table.transform;
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        allStacksBuilt = true;
    }

    // focus on first hit block if raycasting
    //private void OnRaycastBlocks(List<StackBlock> hitBlocks, float totalDamage)
    //{
    //    if (hitBlocks != null && hitBlocks.Count > 0)
    //    {
    //        saveLookAt = orbitCamera.LookAt;
    //        saveFollow = orbitCamera.Follow;

    //        orbitCamera.LookAt = hitBlocks[0].transform;
    //        orbitCamera.Follow = hitBlocks[0].transform;
    //    }
    //    else if (saveLookAt != null && saveFollow != null)
    //    {
    //        orbitCamera.LookAt = saveLookAt;
    //        orbitCamera.Follow = saveFollow;

    //        saveLookAt = null;
    //        saveFollow = null;
    //    }
    //}
}
