using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[CreateAssetMenu(menuName = "Rune Library", fileName = "Rune Library")]

public class RuneLibrary : ScriptableObject
{
    public List<RuneData> runes;
    public Dictionary<RuneData.RunePower, List<RuneData>> runeDict = new();     // runes grouped by power

    public RuneData RuneByName(string runeName) => runes.FirstOrDefault((r) => r.Name == runeName);
    public List<RuneData> RunesByPower(RuneData.RunePower power) => runeDict[power];
    public List<string> RuneNames => runes.Select(r => r.Name).Distinct().ToList();

    public string RandomRune => RuneNames[UnityEngine.Random.Range(0, RuneNames.Count - 1)];


    private void OnEnable()
    {
        BuildDictionary();
    }

    private void BuildDictionary()
    {
        foreach (RuneData.RunePower runePower in Enum.GetValues(typeof(RuneData.RunePower)))
        {
            var powerRunes = runes.Where(r => r.Power == runePower).ToList();

            if (runeDict.ContainsKey(runePower))
                runeDict[runePower].AddRange(powerRunes);
            else
                runeDict.Add(runePower, powerRunes);
        }
    }
}
