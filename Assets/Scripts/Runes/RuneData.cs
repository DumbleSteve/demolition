
// data for easter eggs hidden in blocks
// could be used to represent Battleship-type elements to uncover - ie. in a row or shape of adjoining blocks
// eg. row of same category runes

using System;
using UnityEngine;

[System.Serializable]
public class RuneData
{
    public enum RunePower
    {
        Nothing = 0,
        PlayerCharge,                   // increase/decrease charge, therefore time for player's turn
        StackStrength,                  // increase/decrease strength of all blocks in stack
        FreezeStack,                    // stack cannot be selected until player's turn comes round again
        StackRestore,                   // all blocks restored to full strength
        ExtraHit,                      // go again - another raycast hit -> block damage
    }
    public RunePower Power = RunePower.Nothing;

    public string Name;
    public int MinValue;              // eg. charge / strength increase/decrease percent, etc
    public int MaxValue;              // eg. charge / strength increase/decrease percent, etc
    public int ValueRoundTo;          // for random value - if zero with non-zero Min/MaxValue, then rune value is either Min/MaxValue

    public int ExpiryTime;            // (seconds) 0 == immediate expiry on activation

    public int MinActivationCharge;   // min required to activate
    public int MaxActivationCharge;   // max required to activate
    public int ChargeRoundTo;         // for random value

    public bool IsCollectible;          // crystals, tokens, coins, stars, gems, etc -> TODO: inventory

    public Color RuneTextColour = Color.magenta;        // for UI
    public Rune RunePrefab;             // instantiate in scene when block is built - reveal when demolished, activate when detonated

    //public static int NumRunePowers => Enum.GetNames(typeof(RunePower)).Length - 1;         // excludes RunePower.Nothing

    public static void GetPowerLabel(RunePower power, out string runePower, out string runeDescription)
    {
        switch (power)
        {
            case RunePower.Nothing:
            default:
                runePower = runeDescription = "??? Rune Power";
                break;

            case RunePower.PlayerCharge:
                runePower = "Player Charge";
                runeDescription = "Increase or Reduce Player Detonation Charge";
                break;

            case RunePower.StackStrength:
                runePower = "Stack Strength";
                runeDescription = "Increase or Reduce Strength of all Blocks";
                break;

            case RunePower.FreezeStack:
                runePower = "Freeze Stack";
                runeDescription = "Freeze entire Stack until next Turn";
                break;

            case RunePower.StackRestore:
                runePower = "Restore Stack";
                runeDescription = "Fully Restore all Blocks in Stack";
                break;

            case RunePower.ExtraHit:
                runePower = "Bonus Hits";
                runeDescription = "Increase or Reduce Bonus Detonations during Turn";
                break;
        }
    }
}
