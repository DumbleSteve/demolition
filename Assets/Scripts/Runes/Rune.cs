using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Rune : MonoBehaviour
{
    [SerializeField] private MeshRenderer runeMesh;             // set in inspector
    [SerializeField] private ParticleSystem particles;          // set in inspector
    [SerializeField] private ParticleSystem activateParticles;  // set in inspector

    [SerializeField] private Material selectedMaterial;         // set in inspector

    [SerializeField] private AudioClip revealedAudio;           // set in inspector
    [SerializeField] private AudioClip activatedAudio;          // set in inspector
    [SerializeField] private AudioClip expiredAudio;            // set in inspector

    public RuneData Data { get; private set; }
    public int RuneValue { get; private set; } = 0;             // strength, charge, etc - random as per data min/max
    public int RuneMinCharge { get; private set; } = 0;         // to activate - random as per data min/max
    //public int RuneMinExpiry { get; private set; } = 0;   // as per data

    public int BlockId { get; private set; } = 0;              // unique to each block, so unique to each rune

    private float expiryTimeRemaining;
    private bool expiring;
    private float expireTime = 0.5f;
    private float expireScale = 1.5f;

    private readonly float rotateSpeed = 0.05f;                 // in update
    private Vector3 rotateAngle;                                // random

    private StackBlock parentBlock;
    public Stack ParentStack => parentBlock.ParentStack;

    private bool selected;      // for detonation / activation

    private Collider runeCollider;
    private Renderer runeRenderer;
    private Material runeMaterial;         // original


    public enum RuneState
    {
        Dormant,
        Revealed,
        Activated,
        Expired
    };
    public RuneState CurrentState { get; private set; } = RuneState.Dormant;


    private void Awake()
    {
        runeCollider = GetComponent<Collider>();
        runeRenderer = GetComponentInChildren<Renderer>();

        if (runeRenderer != null)
            runeMaterial = runeRenderer.material;
        else
            Debug.LogError($"Rune '{name}' has no Renderer!");
    }

    private void OnEnable()
    {
        GameEvents.OnRaycastHit += OnRaycastHit;            // hilight / select if this rune
        GameEvents.OnActivateRune += OnActivateRune;        // activate selected runes
    }

    private void OnDisable()
    {
        GameEvents.OnRaycastHit -= OnRaycastHit;
        GameEvents.OnActivateRune -= OnActivateRune;
    }

    public void Init(RuneData libraryData, StackBlock parent, Vector3 runePosition)
    {
        Data = libraryData;
        BlockId = parent.BlockData.blockId;

        // set random value for RuneValue
        if (Data.MinValue != 0 && Data.MaxValue > Data.MinValue && Data.ValueRoundTo > 0)          // Data.MinValue can be negative
        {
            RuneValue = Random.Range(Data.MinValue, Data.MaxValue);
            // round to nearest ValueRoundTo
            RuneValue = (int)Mathf.Round((RuneValue / Data.ValueRoundTo) * Data.ValueRoundTo);     
            //Debug.Log($"Rune.Init '{name}': MinValue {Data.MinValue} MaxValue {Data.MaxValue} RuneValue {RuneValue}");
        }
        // if ValueRoundTo is zero, RuneValue is either Min or MaxValue 
        else if (Data.ValueRoundTo == 0 && (Data.MinValue != 0 || Data.MaxValue != 0))              // Data.MinValue can be negative
        {
            bool isMax = Random.Range(0f, 1f) > 0.5f;   // coin toss
            RuneValue = isMax ? Data.MaxValue : Data.MinValue;
            //Debug.Log($"Rune.Init '{name}': MinValue {Data.MinValue} MaxValue {Data.MaxValue} RuneValue {RuneValue}");
        }

        if (RuneValue == 0)
            RuneValue = Data.MinValue;          // may still be zero!

        // set random value for RuneMinCharge
        if (Data.MinActivationCharge > 0 && Data.MaxActivationCharge > Data.MinActivationCharge && Data.ChargeRoundTo > 0)
        {
            RuneMinCharge = Random.Range(Data.MinActivationCharge, Data.MaxActivationCharge);
            // round to nearest ChargeRoundTo
            RuneMinCharge = (int)Mathf.Round((RuneMinCharge / Data.ChargeRoundTo) * Data.ChargeRoundTo);     
            //Debug.Log($"Rune.Init '{name}': MinActivationCharge {Data.MinActivationCharge} MaxActivationCharge {Data.MaxActivationCharge} RuneMinCharge {RuneMinCharge}");
        }

        if (RuneMinCharge == 0)
            RuneMinCharge = Data.MinActivationCharge;         // may still be zero!

        // initialise other variables
        parentBlock = parent;
        transform.position = runePosition;      // centre of block
        rotateAngle = Random.rotation.eulerAngles;

        runeMesh.enabled = false;
        runeCollider.enabled = false;           // so can't raycast until revealed

        name = $"{ParentStack.StackName}-{libraryData.Power}";
    }

    private void Update()
    {
        if (CurrentState == RuneState.Dormant || CurrentState == RuneState.Expired)
            return;

        transform.Rotate(rotateAngle, rotateSpeed);

        // start expiry countdown if activated
        if (CurrentState == RuneState.Activated)
        {
            if (expiryTimeRemaining > 0)
            {
                expiring = true;
                expiryTimeRemaining -= Time.deltaTime;
            }

            // time's up!
            if (expiring && expiryTimeRemaining <= 0)
            {
                Expire(false);
            }
        }
    }

    public void Reveal()
    {
        if (CurrentState == RuneState.Revealed)
            return;

        CurrentState = RuneState.Revealed;
        runeMesh.enabled = true;
        runeCollider.enabled = true;
        runeCollider.isTrigger = true;

        if (particles != null)
            particles.Play();

        if (Data.ExpiryTime > 0)
            expiryTimeRemaining = Data.ExpiryTime;      // counts down

        // revealedAudio played after scale up...

        GameEvents.OnRuneRevealed?.Invoke(this);
    }

    public void Activate()
    {
        if (CurrentState != RuneState.Revealed)
            return;

        CurrentState = RuneState.Activated;

        //if (runeRenderer != null)
        //    currentMaterial = runeRenderer.material = activatedMaterial;

        if (activateParticles != null)
            activateParticles.Play();

        if (activatedAudio != null)
            AudioSource.PlayClipAtPoint(activatedAudio, transform.position);

        GameEvents.OnRuneActivated?.Invoke(this);

        if (Data.ExpiryTime <= 0)           // expire immediately
        {
            Expire(true);
        }
    }

    public void Expire(bool silent)
    {
        expiryTimeRemaining = 0;
        expiring = false;
        CurrentState = RuneState.Expired;

        runeMesh.enabled = false;
        runeCollider.enabled = false;           // so can't raycast it again

        if (particles != null)
            particles.Stop();

        if (!silent && expiredAudio != null)
            AudioSource.PlayClipAtPoint(expiredAudio, transform.position);

        GameEvents.OnRuneExpired?.Invoke(this);
    }

    private void OnRaycastHit(List<StackBlock> hitBlocks, Rune hitRune)
    {
        selected = hitRune == this && !hitRune.ParentStack.StackDemolished;

        if (selected)
            runeRenderer.material = selectedMaterial;
        else
            runeRenderer.material = runeMaterial;
    }

    private void OnActivateRune()
    {
        if (selected)
            Activate();
    }

    public void ScaleUp(float scaleFactor, float scaleTime)
    {
        LeanTween.scale(gameObject, transform.localScale * scaleFactor, scaleTime)
                    .setEaseOutBounce()
                    .setOnComplete(() =>
                    {
                        var particleShape = particles.shape;
                        particleShape.scale *= scaleFactor;

                        if (revealedAudio != null)
                            AudioSource.PlayClipAtPoint(revealedAudio, transform.position);
                    });
    }

    public void EnableCollider()
    {
        runeCollider.isTrigger = false;
    }

    //private void SetMaterial()
    //{
    //    switch (CurrentState)
    //    {
    //        case RuneState.Dormant:
    //            runeRenderer.material = runeMaterial;
    //            break;
    //        case RuneState.Revealed:
    //            runeRenderer.material = runeMaterial;
    //            break;
    //        case RuneState.Activated:
    //            runeRenderer.material = activatedMaterial;
    //            break;
    //        case RuneState.Expired:
    //            runeRenderer.material = expiredMaterial;
    //            break;
    //    }
    //}
}
