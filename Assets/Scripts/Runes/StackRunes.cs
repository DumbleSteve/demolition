using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// manages the active runes for a stack
// maintains runes in RuneSlots (one for each rune power) in front of parentStack

public class StackRunes : MonoBehaviour
{
    [SerializeField] private Stack parentStack;             // set in inspector
    [SerializeField] private List<RuneSlot> runeSlots;      // set in inspector
    public RuneSlot StackRuneSlot(RuneData.RunePower powerSlot) => runeSlots.FirstOrDefault(x => x.RunePower == powerSlot);

    [SerializeField] private Light runeSpotLight;           // when stack active
    [SerializeField] private AudioClip runeSlotAudio;       // when slot shown

    private List<Rune> revealedRunes = new();
    private List<Rune> activatedRunes = new();
    private List<Rune> expiredRunes = new();
    public int StackRuneCount => revealedRunes.Count + activatedRunes.Count + expiredRunes.Count;

    private float activationScale = 1.4f;
    private float slotShowInterval = 0.15f;

    private float flyHeight = 0.2f;         // extra height above stack height, when revealed
    private float flyTime = 0.4f;
    private float flyPause = 0.1f;          // pause at max Y position


    private void OnEnable()
    {
        GameEvents.OnStackBuilt += OnStackBuilt;
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;
        GameEvents.OnRuneRevealed += OnRuneRevealed;
        GameEvents.OnRuneActivated += OnRuneActivated;
        GameEvents.OnRuneExpired += OnRuneExpired;
    }

    private void OnDisable()
    {
        GameEvents.OnStackBuilt -= OnStackBuilt;
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnRuneRevealed -= OnRuneRevealed;
        GameEvents.OnRuneActivated -= OnRuneActivated;
        GameEvents.OnRuneExpired -= OnRuneExpired;
    }

    private void Start()
    {
        HideSlots();
        runeSpotLight.enabled = false;
    }

    // rune slots need to know stack cardinal (rotation) in order to position runes correctly
    private void OnStackBuilt(Stack newStack)
    {
        if (parentStack != newStack)
            return;

        foreach (var slot in runeSlots)
        {
            slot.SetStack(parentStack);
        }
    }

    private void OnRuneRevealed(Rune rune)
    {
        if (rune.ParentStack != parentStack)        // not interested in other stacks!
            return;

        revealedRunes.Add(rune);

        RuneSlot runeSlot = StackRuneSlot(rune.Data.Power);
        if (runeSlot.Capacity <= 0)
        {
            //GameEvents.OnRunePowerCapacity?.Invoke(rune);     // TODO:  UI message?
            rune.Expire(false);      
            return;
        }

        var runeSlotPosition = runeSlot.AddRune(rune);

        LeanTween.moveY(rune.gameObject, parentStack.CurrentLevelY + flyHeight, flyTime / 2f)
                    .setEaseOutQuad()
                    .setOnComplete(() =>
                    {
                        // down again
                        LeanTween.moveY(rune.gameObject, runeSlotPosition.y, flyTime / 2f)
                                    .setDelay(flyPause)
                                    .setEaseInQuad()
                                    .setOnComplete(() => rune.EnableCollider());    // so fallen blocks don't fall through it!
                    });

        LeanTween.moveX(rune.gameObject, runeSlotPosition.x, flyTime)
                    .setEaseLinear();

        LeanTween.moveZ(rune.gameObject, runeSlotPosition.z, flyTime)
                    .setEaseLinear();

        rune.ScaleUp(activationScale, flyTime);
    }

    private void OnRuneActivated(Rune rune)
    {
        if (rune.ParentStack != parentStack)
            return;

        revealedRunes.Remove(rune);
        activatedRunes.Add(rune);

        switch (rune.Data.Power)
        {
            case RuneData.RunePower.Nothing:
            default:
                break;

            case RuneData.RunePower.PlayerCharge:
                GameEvents.OnChangeMaxCharge?.Invoke(parentStack, (int)rune.RuneValue);
                break;

            case RuneData.RunePower.StackStrength:
                GameEvents.OnChangeBlockStrength?.Invoke(parentStack, rune.RuneValue);
                break;

            case RuneData.RunePower.FreezeStack:
                GameEvents.OnFreezeStack?.Invoke(parentStack, true);
                break;

            case RuneData.RunePower.StackRestore:
                GameEvents.OnRestoreBlocks?.Invoke(parentStack, true);
                break;

            case RuneData.RunePower.ExtraHit:
                GameEvents.OnChangeExtraHits?.Invoke(parentStack, (int)rune.RuneValue);
                break;
        }
    }

    private void OnRuneExpired(Rune rune)
    {
        if (rune.ParentStack != parentStack)
            return;

        activatedRunes.Remove(rune);
        expiredRunes.Add(rune);
    }

    // interval between each slot for visual effect
    public IEnumerator ShowSlots()
    {
        foreach (var slot in runeSlots)
        {
            slot.gameObject.SetActive(true);

            if (runeSlotAudio != null)
                AudioSource.PlayClipAtPoint(runeSlotAudio, transform.position);

            yield return new WaitForSeconds(slotShowInterval);
        }
    }

    public void HideSlots()
    {
        foreach (var slot in runeSlots)
        {
            slot.gameObject.SetActive(false);
        }
    }

    private void OnCameraOrbitStack(Stack stack)
    {
        runeSpotLight.enabled = stack == parentStack;
    }
}
