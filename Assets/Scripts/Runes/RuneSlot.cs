using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RuneSlot : MonoBehaviour
{
    [SerializeField] public RuneData.RunePower RunePower = RuneData.RunePower.Nothing;     // set in inspector

    private int RuneCapacity = 3;                   // max runes in slot
    private List<Rune> runesInSlot = new();         // all runes that have been revealed, activated or expired

    // only non-expired runes shown in slot
    public List<Rune> ActiveRunes => runesInSlot.Where(x => x.CurrentState != Rune.RuneState.Expired).ToList();
    public int Capacity => RuneCapacity - ActiveRunes.Count;

    private float runeSlotSpacing = 0.13f;          // runes arranged in direction of slot
    private float runePositionY = 0.025f;             // runes raised up slightly
    private float rearrangeTime = 0.15f;            // after a rune in this slot expires

    private Stack parentStack;


    private void OnEnable()
    {
        GameEvents.OnRuneExpired += OnRuneExpired;
    }

    private void OnDisable()
    {
        GameEvents.OnRuneExpired -= OnRuneExpired;
    }

    // returns position in slot of new rune (for leantween)
    public Vector3 AddRune(Rune rune)
    {
        if (Capacity <= 0)
            return Vector3.zero;

        runesInSlot.Add(rune);
        return RuneSlotPosition();
    }

    // assumes new rune has already been added to slot
    private Vector3 RuneSlotPosition()
    {
        int lastRuneIndex = ActiveRunes.Count - 1;
        var slotPosition = transform.position;

        switch (parentStack.Cardinal)
        {
            case Stack.StackCardinal.East:
                return new(slotPosition.x - (lastRuneIndex * runeSlotSpacing), slotPosition.y + runePositionY, slotPosition.z);

            case Stack.StackCardinal.South:
                return new(slotPosition.x, slotPosition.y + runePositionY, slotPosition.z + (lastRuneIndex * runeSlotSpacing));

            case Stack.StackCardinal.West:
                return new(slotPosition.x + (lastRuneIndex * runeSlotSpacing), slotPosition.y + runePositionY, slotPosition.z);

            case Stack.StackCardinal.North:
                return new(slotPosition.x, slotPosition.y + runePositionY, slotPosition.z - (lastRuneIndex * runeSlotSpacing));

            default:
                return slotPosition;
        }
    }

    // rearrange runes in this slot if it contains expired rune
    private void OnRuneExpired(Rune rune)
    {
        if (! runesInSlot.Contains(rune))
            return;

        ReArrange();
    }

    // slide runes along slot to fill gaps made by expired runes
    private void ReArrange()
    {
        var activeRunes = ActiveRunes;      // single linq call
        float runePos;

        for (int i = 0; i < activeRunes.Count; i++)
        {
            var rune = activeRunes[i].gameObject;
            var runeSpacing = i * runeSlotSpacing;

            switch (parentStack.Cardinal)
            {
                case Stack.StackCardinal.East:
                    runePos = transform.position.x - runeSpacing;
                    LeanTween.moveX(rune, runePos, rearrangeTime);
                    break;

                case Stack.StackCardinal.South:
                    runePos = transform.position.z + runeSpacing;
                    LeanTween.moveZ(rune, runePos, rearrangeTime);
                    break;

                case Stack.StackCardinal.West:
                    runePos = transform.position.x + runeSpacing;
                    LeanTween.moveX(rune, runePos, rearrangeTime);
                    break;

                case Stack.StackCardinal.North:
                    runePos = transform.position.z - runeSpacing;
                    LeanTween.moveZ(rune, runePos, rearrangeTime);
                    break;
            }
        }
    }

    // rune slots need to know parent stack cardinal (rotation)
    // in order to position rows of runes correctly
    public void SetStack(Stack parentStack)
    {
        this.parentStack = parentStack;
    }
}
